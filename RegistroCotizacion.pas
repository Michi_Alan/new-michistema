unit RegistroCotizacion;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Edit,
  FMX.DateTimeCtrls, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation;

type
  TFrmRegistroCotizacion = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    lbs_Nombre: TLabel;
    lbs_razon: TLabel;
    cb_iva: TComboBox;
    edt_fecha: TDateEdit;
    cb_clientes: TComboBox;
    lbs_regimen: TLabel;
    lbs_RFC: TLabel;
    lbs_producto: TLabel;
    cb_productos: TComboBox;
    edt_cantidad: TEdit;
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    Rectangle1: TRectangle;
    btn_cancelar: TCornerButton;
    btn_agregar: TCornerButton;
    btn_a�adir: TCornerButton;
    btn_borrar: TCornerButton;
    Label1: TLabel;
    procedure cargardatos();
       procedure limpiardatos();
    procedure btn_a�adirClick(Sender: TObject);
    procedure btn_borrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
    row: integer;
  end;

implementation
 uses DataModule ;
{$R *.fmx}



//////////////////////////////    btn a�adir  ////////////////////////////////


procedure TFrmRegistroCotizacion.btn_a�adirClick(Sender: TObject);
begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT precio_unico, descuento FROM productos where nombre = :name;';
  CONN.SQL_SELECT.ParamByName('name').AsString := cb_productos.Selected.Text;

  CONN.SQL_SELECT.Open;

  StringGrid1.Cells[0, row] := cb_productos.Selected.Text;
  StringGrid1.Cells[1, row] := edt_cantidad.Text;
  StringGrid1.Cells[2, row] := CONN.SQL_SELECT.FieldByName('precio_unico').AsString;
  StringGrid1.Cells[3, row] := CONN.SQL_SELECT.FieldByName('descuento').AsString;
  StringGrid1.Cells[4, row] := (strToint(edt_cantidad.Text)* CONN.SQL_SELECT.FieldByName('precio_unico').AsInteger).ToString;
  row := row+1;
end;




 ////////////////// btn boraar ////////////////////////

procedure TFrmRegistroCotizacion.btn_borrarClick(Sender: TObject);
begin
  StringGrid1.RowCount := 0;
  StringGrid1.RowCount := 100;
  row := 0;
end;




/////////////////// Cargar Datos ////////////////////

procedure TFrmRegistroCotizacion.Cargardatos;
begin

  edt_fecha.Date := Now;
  row:= 0;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT nombre FROM clientes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_clientes.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT nombre FROM productos;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_productos.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT porcentaje FROM iva;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_iva.Items.Add(CONN.SQL_SELECT.FieldByName('porcentaje').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;


//////////////// limpiar datos ////////////////////

procedure TFrmRegistroCotizacion.limpiardatos;
begin
  StringGrid1.RowCount := 0;
  StringGrid1.RowCount := 100;
  row := 0;

  edt_cantidad.text := '';
  cb_iva.Items.Clear;
  cb_clientes.Items.Clear;
  cb_productos.Items.Clear;
end;


end.