unit IndexProductos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Objects, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Grid, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.Grid, Data.Bind.DBScope, Articulos;

type
  TFrm_IndexProductos = class(TFrame)
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Rectangle3: TRectangle;
    Grid_Productos: TGrid;
    Titulo_productos: TLabel;
    Line1: TLine;
    btn_agregar: TCornerButton;
    btn_eliminar: TCornerButton;
    btn_modificar: TCornerButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Frame_productos_articulos1: TFrame_productos_articulos;
    procedure btn_eliminarClick(Sender: TObject);
    procedure Frame_productos_articulos1btn_cancelarClick(Sender: TObject);
    procedure Frame_productos_articulos1btn_actualizarClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure btn_modificarClick(Sender: TObject);
    procedure Grid_ProductosCellClick(const Column: TColumn;
      const Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
     procedure cargar_productosTabla;
      var
      id: integer;
  end;

implementation

{$R *.fmx}

uses DataModule;

procedure TFrm_IndexProductos.btn_agregarClick(Sender: TObject);
begin
Frame_productos_articulos1.Visible := True;
Frame_productos_articulos1.cargar_proveedores;
Frame_productos_articulos1.cargar_categoria_productos;
Frame_productos_articulos1.cargar_tipo;
Frame_productos_articulos1.cargar_unidad;
Frame_productos_articulos1.btn_guardar.Visible := True;
Frame_productos_articulos1.btn_actualizar.Visible := False;
end;

procedure TFrm_IndexProductos.btn_eliminarClick(Sender: TObject);
begin
//mensaje de eliminar
 if MessageDlg('Desea Eliminar?',TMsgDlgType.mtConfirmation, mbYesNo, 0)= 6 then

 begin

     CONN.Eliminar_productos;
      btn_eliminar.Enabled := False;
      btn_modificar.Enabled := False;
      btn_agregar.Enabled := True ;
 end;

end;

procedure TFrm_IndexProductos.btn_modificarClick(Sender: TObject);
begin
Frame_productos_articulos1.Visible := True;
Frame_productos_articulos1.cargar_proveedores;
Frame_productos_articulos1.cargar_categoria_productos;
Frame_productos_articulos1.cargar_tipo;
Frame_productos_articulos1.cargar_unidad;
Frame_productos_articulos1.cargardatos_productos;
Frame_productos_articulos1.btn_guardar.Visible := False;
Frame_productos_articulos1.btn_actualizar.Visible := True;
end;

procedure TFrm_IndexProductos.cargar_productosTabla;
begin
 CONN.UniQuery2.SQL.Clear;

     conn.UniQuery2.SQL.Add('SELECT p.id, p.nombre, pro.nombre as Proveedor, ' +
     'c.nombre as Categoria, t.nombre as Tipo, u.nombre as unidades_medida, ' +
     'p.cantidad, p.marca, p.precio_mayoreo, p.precio_unico FROM productos as p, ' +
     'categoria_productos as c, proveedores as pro, tipo_productos as t, ' +
     'unidades_medidas as u WHERE p.id_proveedor = pro.id and p.id_categoria = ' +
     'c.id and p.tipo = t.id and p.unidad_medida = u.id and p.estatus = 1 ORDER BY p.id');
     CONN.UniQuery2.Open;
end;

procedure TFrm_IndexProductos.Frame_productos_articulos1btn_actualizarClick(
  Sender: TObject);
begin
  Frame_productos_articulos1.Visible := False;
  Frame_productos_articulos1.btn_actualizarClick(Sender);
  cargar_productosTabla;

end;

procedure TFrm_IndexProductos.Frame_productos_articulos1btn_cancelarClick(
  Sender: TObject);
begin
Frame_productos_articulos1.Visible := False;
Frame_productos_articulos1.limpiar_campo;
cargar_productosTabla;

end;

procedure TFrm_IndexProductos.Grid_ProductosCellClick(const Column: TColumn;
  const Row: Integer);
begin
id :=  Row+1;
btn_agregar.Enabled := False;
btn_modificar.Enabled := True;
btn_eliminar.Enabled := True;
end;

end.

