unit IndexSucursales;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, FMX.Objects, RegistroSuc,
  DataModule,FMX.Controls.Presentation;

type
  TFrame_Sucursal = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_sucursales: TStringGrid;
    frm_registrosuc1: Tfrm_registrosuc;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    StringColumn7: TStringColumn;
    StringColumn8: TStringColumn;
    StringColumn9: TStringColumn;
    StringColumn10: TStringColumn;
    procedure btn_agregarClick(Sender: TObject);
    procedure cargartabla;
    procedure btn_configurarClick(Sender: TObject);
    procedure Grd_SucursalesCellClick(const Column: TColumn; const Row: Integer);
    procedure frm_registrosuc1btn_desactivarClick(Sender: TObject);
    procedure frm_registrosuc1btn_configurarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrame_Sucursal.btn_agregarClick(Sender: TObject);
begin
   frm_registrosuc1.Visible := True;
   frm_registrosuc1.cargardatos;
end;
procedure TFrame_Sucursal.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.nombre, a.direccion, a.telefono, a.colonia, a.CP, b.nombre as "empresa", c.nombre as "muni", d.nombre as "est", e.nombre as "pais" FROM sucursales a'
    +' JOIN empresa b ON a.id_empresa = b.id'
    +' JOIN municipios c ON a.id_municipio = c.id'
    +' JOIN estados d ON a.id_estado = d.id'
    +' JOIN paises e ON a.id_pais = e.id';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin

          grd_sucursales.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          grd_sucursales.Cells[1, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
          grd_sucursales.Cells[2, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
          grd_sucursales.Cells[3, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
          grd_sucursales.Cells[4, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          grd_sucursales.Cells[5, I] := CONN.SQL_SELECT.FieldByName('empresa').AsString;
          grd_sucursales.Cells[6, I] := CONN.SQL_SELECT.FieldByName('muni').AsString;
          grd_sucursales.Cells[7, I] := CONN.SQL_SELECT.FieldByName('est').AsString;
          grd_sucursales.Cells[8, I] := CONN.SQL_SELECT.FieldByName('pais').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure TFrame_Sucursal.frm_registrosuc1btn_configurarClick(Sender: TObject);
begin
  frm_registrosuc1.Visible:= False;
  cargartabla;
end;

procedure TFrame_Sucursal.frm_registrosuc1btn_desactivarClick(Sender: TObject);
var
  id_empresa,id_estado, id_pais, id_municipio: integer;
  nombre, tel, direc, col, cp: String;
begin
    {
      Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
      si este fuera el caso podra la variable como vacia y no podr� pasar la
      siguiente condici�n.
    }

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM paises WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_registrosuc1.cb_pais.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_pais := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM estados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_registrosuc1.cb_estados.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_estado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM municipios WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_registrosuc1.cb_Municipio.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_municipio := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM empresa WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_registrosuc1.cb_Empresa.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_empresa := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;


      nombre := frm_registrosuc1.edt_Nombre.Text;
      tel := frm_registrosuc1.edt_telefono.Text;
      direc := frm_registrosuc1.edt_Direccion.Text;
      col :=  frm_registrosuc1.edt_Colonia.text;
      cp := frm_registrosuc1.edt_CP.Text;

    //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

    if (id_estado = 0 ) and (id_municipio = 0) and (id_pais = 0)and (id_empresa = 0) or (nombre = '')
         or (col = '') or (cp = '') or
        (direc = '') or (tel = '')  then
    begin
      ShowMessage('�Los campos no pueden estar vacios!');
    end
    else
    begin

      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'INSERT INTO sucursales (nombre, RFC, direccion, telefono, colonia, CP, id_empresa, id_municipio, id_estado, id_pais) VALUES (:nombre, :rfc, :direc, :tel, :col, :cp, :empresa, :muni, :estado, :pais)';

      CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
      CONN.SQL_SELECT.ParamByName('tel').AsString := tel;
      CONN.SQL_SELECT.ParamByName('direc').AsString := direc;
      CONN.SQL_SELECT.ParamByName('col').AsString := col;
      CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
      CONN.SQL_SELECT.ParamByName('empresa').AsString := id_empresa.ToString;
      CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
      CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
      CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
      CONN.SQL_SELECT.ExecSQL;

      if (CONN.SQL_SELECT.RowsAffected > 0) then
      begin
        ShowMessage('�Se ha registrado la informaci�n!');
        frm_registrosuc1.Visible:= False;
        cargartabla;
      end
      else
      begin
        ShowMessage('Algo ha salido mal...');
      end;

  end;
end;

procedure TFrame_Sucursal.btn_configurarClick(Sender: TObject);
begin
  frm_registrosuc1.Visible := False;
  frm_registrosuc1.limpiardatos;
  self.cargartabla;
end;

procedure TFrame_Sucursal.Grd_SucursalesCellClick(const Column: TColumn;
  const Row: Integer);
begin
  ShowMessage(Row.ToString);
end;


end.
