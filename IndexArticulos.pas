unit IndexArticulos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, FMX.Objects,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.Grid, Data.Bind.DBScope, Data.DB, Articulos;

type
  TFrame_articulo = class(TFrame)
    index_articulo: TRectangle;
    Contenidop: TRectangle;
    Line1: TLine;
    StringGrid1: TStringGrid;
    btn_agregar: TCornerButton;
    Titulo_productos: TLabel;
    btn_eliminar_articulo: TCornerButton;
    btn_modificar: TCornerButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Frame_productos_articulos1: TFrame_productos_articulos;

    procedure btn_eliminar_articuloClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure Frame_productos_articulos1btn_cancelarClick(Sender: TObject);
    procedure Frame_productos_articulos1btn_guardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses DataModule;

procedure TFrame_articulo.btn_agregarClick(Sender: TObject);
//Para que muestra el formulario
begin
  //CONN.Nuevo_productos;
  Frame_productos_articulos1.Visible := True;
end;

procedure TFrame_articulo.btn_eliminar_articuloClick(Sender: TObject);
begin
//mensaje de eliminar
 if MessageDlg('Desea Eliminar?',TMsgDlgType.mtConfirmation, mbYesNo, 0)= 6 then

 begin
 //llamar el dataMule con el nombre conn
  //CONN.Eliminar_productos;
      //btn_eliminar_articulo.Enabled := False;
  //     btn_modificar.Enabled := False;
  //     btn_guardar.Enabled := True ;
 end;


end;



procedure TFrame_articulo.Frame_productos_articulos1btn_cancelarClick(
  Sender: TObject);
begin
    Frame_productos_articulos1.Visible := False;
    Frame_productos_articulos1.limpiar_campo;
end;

procedure TFrame_articulo.Frame_productos_articulos1btn_guardarClick(
  Sender: TObject);
begin
  Frame_productos_articulos1.btn_guardarClick(Sender);
   Frame_productos_articulos1.Visible := False;

end;

end.
