﻿unit Layout;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Ani,
  System.ImageList, FMX.ImgList, LoginForm, Tablas, Formularios,
  Proveedor, IndexProveedor, Almacenes, RegistroUsuarios, IndexArticulos,
  Articulos, Clientes, IndexEmpresa, IndexSucursales, FMX.ListBox, FMX.Layouts,
  IndexPaises, IndexMunicipios, IndexEstados, RegistroEstado,
  IndexCategoriaProducto, IndexMedidas, Traspasos, michiUtils, Grupos,
  Inventario, OrdenDeCompra, SalidasProd, EntradasProd, IndexUsuarios,
  PedidosVenta, Rutas, IndexProductos, PermisosUsuarios, IndexSubCliente,
  IndexIVA2, IndexOrdenMain;

type
  TForm8 = class(TForm)
    Main_rec: TRectangle;
    top_rec: TRectangle;
    left_rec: TRectangle;
    client_rec: TRectangle;
    rec_btn: TButton;
    cerrar_rec: TFloatAnimation;
    abrir_rec: TFloatAnimation;
    StyleBook1: TStyleBook;
    ImageList1: TImageList;
    lbl_display_user: TLabel;
    btn_salir: TCornerButton;
    frm_login: TFrame1;
    frm_formularios: TFrame9;
    frm_tablas: TFrame10;
    btn_formularios: TCornerButton;
    btn_proveedor: TCornerButton;
    btn_articulos: TCornerButton;
    btn_cliente: TCornerButton;
    frm_articulo: TFrame_articulo;
    frm_almacen: TFrame2;
    frm_empresa: TFrame_Empresa;
    frm_sucursal: TFrame_Sucursal;
    Configuracion_list: TListBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    frm_paises: TFrame_Paises;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem8: TListBoxItem;
    ListBoxItem9: TListBoxItem;
    frm_cliente: TFrame_IndexCliente;
    frm_Estados: Tfrm_IndexEstado;
    frm_Municipio: Tfrm_IndexMunicipio;
    frm_categorias: TIndex_CategoriaProductos;
    Frame_articulo1: TFrame_articulo;
    Servicios_list: TListBox;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    clientes_list: TListBoxItem;
    provee_list: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    categoria_list: TListBoxItem;
    Art_list: TListBoxItem;
    frm_Medidas: Tfrm_indexMedidas;
    ListBoxItem10: TListBoxItem;
    ListBox1: TListBox;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    entradas_list: TListBoxItem;
    salidas_list: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    Transpasos_list: TListBoxItem;
    ListBoxItem14: TListBoxItem;
    ListBoxItem11: TListBoxItem;
    frm_transpasos: TFrame5;
    frm_grupos: TIndexGrupos;
    Index_CategoriaProductos1: TIndex_CategoriaProductos;
    Fam_indexprovedor1: TFam_indexprovedor;
    frm_inventario: TFrame7;
    frm_entrada: TFrame6;
    frm_salidas: TFrame_Salida;
    frm_usuarios: Tfrm_index_usuarios;
    trans_list: TListBox;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    ListBoxItem15: TListBoxItem;
    ListBoxItem16: TListBoxItem;
    Frm_venta: TFrm_IndexVenta;
    btn_cs: TCornerButton;
    frm_rutas: TFrame4;
    frm_productos: TFrm_IndexProductos;
    frm_subclientes: TFrm_IndexSubCliente;
    ListBoxItem17: TListBoxItem;
    frm_permisos: Tfrm_permisosUsusarios;
    Image1: TImage;
    rec_logo: TRectangle;
    frm_IVA: Tfrm_IndexIVA;
    frm_IndexOrden1: Tfrm_IndexOrden;
    procedure btn_salirClick(Sender: TObject);
    procedure rec_btnClick(Sender: TObject);
    procedure btn_formulariosClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_proveedorClick(Sender: TObject);
    procedure Frame_proveedor1btn_cancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Frame_proveedor1btn_guardarClick(Sender: TObject);
    procedure ListBoxItem1Click(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure ListBoxItem3Click(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    procedure ListBoxItem6Click(Sender: TObject);
    procedure ListBoxItem7Click(Sender: TObject);
    procedure DivisasClick(Sender: TObject);
    procedure CornerButton1Click(Sender: TObject);
    procedure frm_empresabtn_agregarClick(Sender: TObject);
    procedure frm_Registrobtn_configurarClick(Sender: TObject);
    procedure frm_Registrobtn_desactivarClick(Sender: TObject);
    procedure clientes_listClick(Sender: TObject);
    procedure provee_listClick(Sender: TObject);
    procedure Art_listClick(Sender: TObject);
    procedure ListBoxItem10Click(Sender: TObject);
    procedure Transpasos_listClick(Sender: TObject);
    procedure btn_articulosClick(Sender: TObject);
    procedure ListBoxItem8Click(Sender: TObject);
    procedure hideAllFrames();
    procedure hideAllList();
    procedure ListBoxItem14Click(Sender: TObject);
    procedure ListBoxItem9Click(Sender: TObject);
    procedure categoria_listClick(Sender: TObject);
    procedure ListBoxItem12Click(Sender: TObject);
    procedure entradas_listClick(Sender: TObject);
    procedure salidas_listClick(Sender: TObject);
    procedure btn_clienteClick(Sender: TObject);
    procedure ListBoxItem15Click(Sender: TObject);
    function login(): Boolean;
    procedure frm_loginCornerButton1Click(Sender: TObject);
    procedure frm_loginCornerButton1KeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure btn_csClick(Sender: TObject);
    procedure frm_loginbtn_testClick(Sender: TObject);
    procedure frm_paisesbtn_agregarClick(Sender: TObject);
    procedure ListBoxItem11Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem17Click(Sender: TObject);
    procedure frm_productosbtn_modificarClick(Sender: TObject);
    procedure Fam_indexprovedor1btn_agregarClick(Sender: TObject);
    procedure Fam_indexprovedor1btn_modificarClick(Sender: TObject);
    procedure ListBoxItem16Click(Sender: TObject);
    procedure frm_usuarioscornbtn_formu2Click(Sender: TObject);
    procedure frm_gruposbtn_agregarClick(Sender: TObject);
    procedure frm_IndexOrden1btn_agregarClick(Sender: TObject);
    procedure frm_Medidasbtn_agregarClick(Sender: TObject);
    procedure frm_IVAbtn_agregarClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;


implementation

uses DataModule;


{$R *.fmx}

function TForm8.login;
  var username, pass, passE: string;
begin

  {
   Las variables <<username>> y <<pass>>, seran asignadas los por TEdit
   contenidos en el Frame <<frm_login>> el cual corresponde al archivo Unit1.pas.

   @Notas
   La variable <<passCom>> por el momento no se utiliza, se har� cuando habilitemos
   la base de datos y se utilizar� para validar el inicio de sesi�n.

   @PARAMS
   username := Dato string obtenido de un TEdit.
   pass     := Dato string obtenido de un TEdit.
   pass     := Dato string obtenido como respuesta de la consulta SQL.
  }

  username:= frm_login.edt_username.Text;
  pass:= frm_login.edt_pass.Text;

  if (pass = '') or (username = '') then
  begin
    ShowMessage('�Los campos de usuario y contrase�a no pueden estar vacios!');
  end
  else
  begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.usuario, a.pass FROM usuarios a WHERE usuario=:username';
               CONN.SQL_SELECT.ParamByName('username').AsString := username;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin
          passE := CONN.SQL_SELECT.FieldByName('pass').AsString;
          if passE = michiUtils.md5(pass) then
          begin
              rec_logo.Visible:=False;
              frm_login.Visible:= False;
              frm_login.edt_username.Text := '';
              frm_login.edt_pass.Text := '';
              hideAllFrames;
              hideAllList;
              michiUtils.newLogin(CONN.SQL_SELECT.FieldByName('id').AsInteger);
          end
          else
          begin
             ShowMessage('Usuario o contraseña incorrecto');
             CONN.SQL_SELECT.Close;
          end;
    end
    else
    begin
      ShowMessage('Usuario o contraseña incorrecto');
      CONN.SQL_SELECT.Close;
    end;



  end;

end;

//VISIBLES TO FALSE
procedure TForm8.hideAllFrames;
begin
  frm_empresa.Visible := False;
  Fam_indexprovedor1.Visible:=False;
  Fam_indexprovedor1.Frame_proveedor1.Visible:= False;
  frm_articulo.Visible := False;
  frm_almacen.Visible := False;
  frm_cliente.Visible := False;
  frm_sucursal.Visible := False;
  frm_paises.Visible := False;
  frm_Estados.Visible := False;
  frm_medidas.Visible := False;
  frm_categorias.Visible := False;
  frm_Municipio.Visible := False;
  frm_grupos.Visible:=False;
  frm_inventario.Visible := False;
  Index_CategoriaProductos1.Visible := False;

  frm_indexOrden1.Visible:=False;
  frm_salidas.Visible := False;
  frm_entrada.Visible := False;
  frm_usuarios.Visible := False;
  frm_venta.Visible := False;
  frm_rutas.Visible := False;
  frm_productos.Visible := False;
  frm_subclientes.Visible := False;
  frm_permisos.Visible := False;
  frm_IVA.Visible := False;
end;

procedure TForm8.hideAllList;
begin
  Configuracion_list.Visible := False;
  Servicios_list.Visible := False;
  ListBox1.Visible := False;
  trans_list.Visible := False;
end;

procedure TForm8.Art_listClick(Sender: TObject);
begin
  hideAllFrames;
  hideAllList;
  //Aqu� se ejecutan procesos internos del Frame
  frm_productos.Visible:= True;
  frm_productos.cargar_productosTabla;


end;

procedure TForm8.btn_formulariosClick(Sender: TObject);
begin

    if Configuracion_list.Visible = True then
    begin
      hideAllList;
    end
    else
    begin
      hideAllList;
      Configuracion_list.Visible := True;
    end;

end;




procedure TForm8.btn_proveedorClick(Sender: TObject);
begin


  if Servicios_list.Visible = True then
  begin
    hideAllList;
  end
  else
  begin
    hideAllList;
    Servicios_list.Visible := True;
  end;

end;

procedure TForm8.btn_salirClick(Sender: TObject);
var
  selected: integer;
begin

  {
    Este es un ShowMessage de tipo YesNoQuestion el cual posee dos botones de s� y no
    En lugar de regresar TRUE o FALSE, dependiendo su respuesta recibira un n�mero.
    Para este caso si el valor de la variable <<selected> es igual a 6 la aplicaci�n
    cerrar�, de lo contrario continuar� normal

    @FUNCI�N
    La funci�n MessageDlg es similar al ShowMessage, pero este contendra botones asignados
    por nosotros y de utiliza de la siguiente manera:

      MessageDlg( mensaje, tipo de dialogo, [Arreglo de botones], 0);

    @NOTAS
    Todas los posibles tipos de dialogos, botones y respuestas se encuentran en el siguiente enlace.
    http://www.delphibasics.co.uk/RTL.asp?Name=messagedlg

    @PARAMS
    selected := Dato integer obtenido de la respuesta de ShowMessage(MessageDlg).
  }

  selected := MessageDlg('�Seguro que quieres salir?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0);
  if selected = 6 then
  begin
    Application.Terminate;
  end
  else
  begin

  end;

end;

procedure TForm8.Button1Click(Sender: TObject);
begin
  if frm_formularios.Visible = True then
  begin
  frm_formularios.Visible := false;
  frm_tablas.Visible := false;
  Fam_indexprovedor1.Frame_proveedor1.Visible := True;
  end
  else
  begin
  Fam_indexprovedor1.Frame_proveedor1.Visible := True;

  end;

end;

procedure TForm8.categoria_listClick(Sender: TObject);
begin
  hideAllFrames;
  Servicios_list.Visible := False;

  Index_CategoriaProductos1.Visible := True;
  Index_CategoriaProductos1.btn_eliminar.Enabled := False;
  Index_CategoriaProductos1.btn_modificar.Enabled := False;
  Index_CategoriaProductos1.btn_guardar.Enabled := True;
end;

procedure TForm8.clientes_listClick(Sender: TObject);
begin
  hideAllFrames;
  Servicios_list.Visible := False;
  //Aqu� se ejecutan procesos internos del Frame
  frm_cliente.Visible := True;
  frm_cliente.cargartabla;
end;

procedure TForm8.CornerButton1Click(Sender: TObject);
begin
//Categrias

Index_CategoriaProductos1.Visible := True;
Index_CategoriaProductos1.Reg_categoriasProducto1.Visible :=False;
//Para que el boton no se puede habilitar antes de que selecione un id
Index_CategoriaProductos1.btn_guardar.Enabled := True;
Index_CategoriaProductos1.btn_eliminar.Enabled := False;
Index_CategoriaProductos1.btn_modificar.Enabled := False;
Frame_articulo1.Visible:= False;

end;

procedure TForm8.DivisasClick(Sender: TObject);
begin
hideAllFrames;
  Configuracion_list.Visible := False;
  frm_iva.Visible := True;
  //Aqu� se ejecutan procesos internos del Frame
  frm_iva.cargartabla;
end;

procedure TForm8.entradas_listClick(Sender: TObject);
begin
  hideAllFrames;
  ListBox1.Visible := False;

  frm_entrada.Visible := True;
end;

procedure TForm8.btn_articulosClick(Sender: TObject);
begin

  if ListBox1.Visible = True then
  begin
    hideAllList;
  end
  else
  begin
    hideAllList;
    ListBox1.Visible := True;
  end;
end;

procedure TForm8.btn_clienteClick(Sender: TObject);
begin

  if trans_list.Visible = True then
  begin
    hideAllList;
  end
  else
  begin
    hideAllList;
    trans_list.Visible := True;
  end;
end;

procedure TForm8.btn_csClick(Sender: TObject);
var selected: integer;
begin
selected := MessageDlg('¿Seguro que quieres cerrar sesión?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0);
  if selected = 6 then
  begin
    frm_login.Visible := True;
    rec_logo.Visible:=True;
    CONN.SQL_SELECT.Close;
    {Main_rec.destroy;}
  end
  else
  begin

  end;
end;

procedure TForm8.Fam_indexprovedor1btn_agregarClick(Sender: TObject);
begin
  Fam_indexprovedor1.btn_agregarClick(Sender);
  Fam_indexprovedor1.Frame_proveedor1.btn_actualizar.Visible := False;
  Fam_indexprovedor1.Frame_proveedor1.btn_guardar.Visible := True;


end;

procedure TForm8.Fam_indexprovedor1btn_modificarClick(Sender: TObject);
begin
  Fam_indexprovedor1.btn_modificarClick(Sender);
  Fam_indexprovedor1.Frame_proveedor1.btn_actualizar.Visible := True;
  Fam_indexprovedor1.Frame_proveedor1.btn_guardar.Visible := False;

end;

procedure TForm8.FormActivate(Sender: TObject);
begin
//para mostrar los paises disponible
//conn.FDQuery_pais.Open();


end;

procedure TForm8.FormCreate(Sender: TObject);
begin
frm_login.Visible := True;
rec_logo.Visible:=True
end;

procedure TForm8.Frame_proveedor1btn_cancelarClick(Sender: TObject);
begin
Fam_indexprovedor1.Frame_proveedor1.Visible:= false;
frm_formularios.Visible := false;
frm_tablas.Visible := false;
fam_indexprovedor1.Visible := true;
Fam_indexprovedor1.Frame_proveedor1.limpiar;
end;

procedure TForm8.Frame_proveedor1btn_guardarClick(Sender: TObject);
begin
   Fam_indexprovedor1.Frame_proveedor1.btn_guardarClick(Sender);

   Fam_indexprovedor1.Visible:= true;
   Fam_indexprovedor1.Frame_proveedor1.Visible:= false;

end;

procedure TForm8.frm_empresabtn_agregarClick(Sender: TObject);
begin
  frm_empresa.btn_agregarClick(Sender);

end;

procedure TForm8.frm_gruposbtn_agregarClick(Sender: TObject);
begin
frm_grupos.frm_registrar.Visible:=True;
frm_grupos.frm_registrar.loadData;
end;

procedure TForm8.frm_IndexOrden1btn_agregarClick(Sender: TObject);
begin
  frm_IndexOrden1.btn_agregarClick(Sender);

end;

procedure TForm8.frm_IVAbtn_agregarClick(Sender: TObject);
begin
  frm_iva.btn_agregarivaClick(Sender);
end;

procedure TForm8.frm_loginbtn_testClick(Sender: TObject);
begin
hideAllFrames;
hideAllList;
rec_logo.Visible:=False;
frm_login.Visible:=False;
end;

procedure TForm8.frm_loginCornerButton1Click(Sender: TObject);
begin
  self.login;
end;

procedure TForm8.frm_loginCornerButton1KeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
  begin
case Key of
  13:
      self.login;
  end;

end;


procedure TForm8.frm_Medidasbtn_agregarClick(Sender: TObject);
begin
  frm_Medidas.btn_agregarClick(Sender);

end;

procedure TForm8.frm_paisesbtn_agregarClick(Sender: TObject);
begin
 frm_paises.frm_registro.Visible := True;
end;

procedure TForm8.frm_productosbtn_modificarClick(Sender: TObject);
begin
  frm_productos.btn_modificarClick(Sender);
   frm_productos.Frame_productos_articulos1.cargardatos_productos;

end;

procedure TForm8.frm_Registrobtn_configurarClick(Sender: TObject);
begin
  frm_empresa.frm_Registrobtn_configurarClick(Sender);

end;

procedure TForm8.frm_Registrobtn_desactivarClick(Sender: TObject);
begin
  frm_empresa.frm_Registrobtn_desactivarClick(Sender);

end;


procedure TForm8.frm_usuarioscornbtn_formu2Click(Sender: TObject);
begin
frm_usuarios.pl_new.Visible := False;
end;

procedure TForm8.ListBoxItem10Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_medidas.Visible := True;
  //Aqu� se ejecutan procesos internos del Frame
  frm_medidas.cargartabla;

end;

procedure TForm8.ListBoxItem11Click(Sender: TObject);
begin
hideAllFrames;
frm_rutas.Visible:= True;
frm_rutas.cargar_rutasdetalles;
hideAllList;
end;

procedure TForm8.ListBoxItem12Click(Sender: TObject);
begin
  hideAllFrames;
  hideAllList;
  //Aqu� se ejecutan procesos internos del Frame

end;

procedure TForm8.ListBoxItem14Click(Sender: TObject);
begin
  hideAllFrames;
  hideAllList;
  //Aqu� se ejecutan procesos internos del Frame
  frm_inventario.Visible:= True;
  frm_inventario.cargartabla;
  frm_inventario.cargardatos;
end;

procedure TForm8.ListBoxItem15Click(Sender: TObject);
begin
  hideAllFrames;
  frm_venta.Visible := True;
  Frm_venta.cargartabla;
  hideAllList;
end;

procedure TForm8.ListBoxItem16Click(Sender: TObject);
begin
  hideAllFrames;
  hideAllList;
  //Aqu� se ejecutan procesos internos del Frame
  frm_indexOrden1.Visible:=true;

end;

procedure TForm8.ListBoxItem17Click(Sender: TObject);
begin
  hideAllFrames;
  hideAllList;
  frm_subclientes.Visible := True;
  frm_subclientes.cargartabla;
end;

procedure TForm8.ListBoxItem1Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_empresa.Visible := True;
  //Aqu� se ejecutan procesos internos del Frame
  frm_empresa.cargartabla;
end;

procedure TForm8.ListBoxItem2Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_sucursal.Visible := True;
  //Inicializamos la tabla
  frm_sucursal.cargartabla;
end;

procedure TForm8.ListBoxItem3Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_almacen.Visible := True;
  frm_almacen.cargartabla;
end;

procedure TForm8.ListBoxItem4Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_usuarios.Visible := True;
  frm_usuarios.cargartabla;
end;

procedure TForm8.ListBoxItem5Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_paises.Visible := True;
  frm_paises.cargartabla;
end;

procedure TForm8.ListBoxItem6Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_Estados.Visible := True;
  frm_Estados.cargartabla;
end;

procedure TForm8.ListBoxItem7Click(Sender: TObject);
begin

  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_Municipio.Visible := True;
  frm_Municipio.cargartabla;

end;

procedure TForm8.ListBoxItem8Click(Sender: TObject);
begin

  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_grupos.Visible := True;
  frm_grupos.cargartabla
end;

procedure TForm8.ListBoxItem9Click(Sender: TObject);
begin
  hideAllFrames;
  Configuracion_list.Visible := False;
  frm_permisos.cargarusuarios;
  frm_permisos.Visible := True;

end;

procedure TForm8.provee_listClick(Sender: TObject);
begin
  hideAllFrames;
  servicios_list.Visible := False;
  //Aqu� se ejecutan procesos internos del Frame
  Fam_indexprovedor1.Visible:=True;
  //Fam_indexprovedor1.cargartabla_proveedor;
  Fam_indexprovedor1.btn_agregar.Enabled := True;
  Fam_indexprovedor1.btn_eliminar.Enabled := False;
  Fam_indexprovedor1.btn_modificar.Enabled := False;
  Fam_indexprovedor1.cargar_proveedor;



end;

procedure TForm8.rec_btnClick(Sender: TObject);
var
  w: Single;
begin

  {
    La variable w se encarga de detectar el ancho del men� lateral en el momento
    de presionarlo, una vez sabiendolo comparar� si este esa abierto o cerrado
    en el caso de medir 100, significar� que este se encuentra abierto y realizar�
    la animaci�n para cerrarla (cerrar.rec), adem�s de ocultar los botones contenidos
    en el men�, si mide 0 har� justo lo contrar�o.

    @NOTAS
    El tipo de datos para obtener la altura y el ancho correspnde a <<Single>>.

    @PARAMS
    w := Dato tipo Single obtenido del ancho de la barra.

  }

  w:= left_rec.Width;
  if w = 100 then
  begin
    cerrar_rec.Start;
    btn_salir.Visible:= False;
  end
  else if w = 0 then
  begin
    abrir_rec.Start;
    btn_salir.Visible:= True;
  end;

end;



procedure TForm8.salidas_listClick(Sender: TObject);
begin
  hideAllFrames;
  ListBox1.Visible := False;

  frm_salidas.Visible := True;
  frm_salidas.cargardatos;
end;

procedure TForm8.Transpasos_listClick(Sender: TObject);
begin

  hideAllFrames;
  hideAllList;
  //Aqu� se ejecutan procesos internos del Frame
  frm_transpasos.Visible := True;
  frm_transpasos.cargartabla;

end;

end.
