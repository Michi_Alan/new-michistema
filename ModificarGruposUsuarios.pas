unit ModificarGruposUsuarios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts, FMX.ListBox;

type
  Tmodificar_grupos_usuarios = class(TFrame)
    Panel2: TPanel;
    lb_selected: TListBox;
    lb_available: TListBox;
    lbl_name: TLabel;
    btn_remove: TButton;
    btn_add: TButton;
    lbl_available: TLabel;
    lbl_selected: TLabel;
    ListBox1: TListBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

end.
