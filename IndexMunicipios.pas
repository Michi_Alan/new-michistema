unit IndexMunicipios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,DataModule,
  RegistroMuni, System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox,
  FMX.Objects, FMX.Controls.Presentation;

type
  Tfrm_IndexMunicipio = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    Grd_Muni: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    frm_registroMuni1: Tfrm_registroMuni;
    btn_agregar: TCornerButton;
    frm_registroMuni2: Tfrm_registroMuni;
    procedure btn_configurarClick(Sender: TObject);
    procedure cargartabla;
    procedure frm_registroMuni1btn_configurarClick(Sender: TObject);
    procedure frm_registroMuni1btn_desactivarClick(Sender: TObject);
    procedure Grd_MuniCellClick(const Column: TColumn; const Row: Integer);
    procedure Grd_MuniCellDblClick(const Column: TColumn; const Row: Integer);
    procedure btn_agregarClick(Sender: TObject);
    procedure frm_registroMuni2btn_configurarClick(Sender: TObject);
    procedure frm_registroMuni2btn_desactivarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
    id: integer;
  end;

implementation

{$R *.fmx}

procedure Tfrm_IndexMunicipio.btn_agregarClick(Sender: TObject);
begin
//AGREGAR
 //Frm_RegsitroMuni2.cargardatos;
  Frm_RegistroMuni2.Visible := True;
end;

procedure Tfrm_IndexMunicipio.btn_configurarClick(Sender: TObject);
begin
 frm_RegistroMuni1.Visible:= True;
 frm_registroMuni1.id := Self.id;
 frm_registroMuni1.cargardatos;
end;
procedure Tfrm_IndexMunicipio.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.clave  FROM municipios a LIMIT 99;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Muni.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Muni.Cells[0, I] := CONN.SQL_SELECT.FieldByName('clave').AsString;
          Grd_Muni.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;


procedure Tfrm_IndexMunicipio.frm_registroMuni1btn_configurarClick(
  Sender: TObject);
begin
  frm_RegistroMuni1.Visible:= False;
  self.cargartabla;
end;

procedure Tfrm_IndexMunicipio.frm_registroMuni1btn_desactivarClick(
  Sender: TObject);
var
clave, nombre: String;
begin

  clave := frm_registroMuni1.edt_clave.Text;
  nombre := frm_registroMuni1.edt_Nombre.Text;

  if (nombre = '') or (clave = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'UPDATE municipios SET nombre= :nombre, clave= :clave WHERE id= :id';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('clave').AsString := clave;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := frm_registroMuni1.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_RegistroMuni1.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
  end;

end;
procedure Tfrm_IndexMunicipio.frm_registroMuni2btn_configurarClick(
  Sender: TObject);
begin
//Agregar
  Frm_RegistroMuni2.Visible:= False;
  self.cargartabla;
  Frm_RegistroMuni2.limpiardatos;
end;

procedure Tfrm_IndexMunicipio.frm_registroMuni2btn_desactivarClick(
  Sender: TObject);
  //AGREgar
var
clave, nombre: String;
begin

  clave := frm_registroMuni1.edt_clave.Text;
  nombre := frm_registroMuni1.edt_Nombre.Text;

  if (nombre = '') or (clave = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO municipios (id,nombre,clave)VALUES(:id,:nombre,:clave)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('clave').AsString := clave;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := frm_registroMuni1.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_RegistroMuni1.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
end;
end;

procedure Tfrm_IndexMunicipio.Grd_MuniCellClick(const Column: TColumn;
  const Row: Integer);
begin
  id :=  Row+1;
  btn_configurar.Enabled := True;
end;


procedure Tfrm_IndexMunicipio.Grd_MuniCellDblClick(const Column: TColumn;
  const Row: Integer);
begin
    id :=  Row+1;
  btn_configurar.Enabled := True;
end;

end.
