﻿unit SalidasProd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.ListBox,
  FMX.DateTimeCtrls, FMX.Edit, FMX.Objects, FMX.Controls.Presentation, DataModule;

type
  TFrame_Salida = class(TFrame)
    rect_container: TRectangle;
    lbl_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    lbl_Alm: TLabel;
    lbl_fecha: TLabel;
    dateedt_fecha: TDateEdit;
    lbl_Obser: TLabel;
    edt_obser: TEdit;
    lbl_client: TLabel;
    combo_client: TComboBox;
    btn_acept: TCornerButton;
    btn_cancel: TCornerButton;
    rec_botones: TRectangle;
    ComboBox1: TComboBox;
    lbl_producto: TText;
    ComboBox2: TComboBox;
    lbl_Detalles: TText;
    btn_agregar: TCornerButton;
    btn_borrar: TCornerButton;
    grd_salidas: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    procedure cargardatos();
    procedure btn_borrarClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure btn_aceptClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
    row: integer
  end;

implementation

{$R *.fmx}

procedure TFrame_Salida.btn_aceptClick(Sender: TObject);
var
  I, id_mov, id_almacen, id_empleado, id_producto: integer;
  observacion: string;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM empleados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := combo_client.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_empleado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end
    else
    begin
      id_empleado := 0;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre = :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := ComboBox1.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_almacen := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end
    else
    begin
      id_almacen := 0;
    end;


  //fecha := frm_registro.edt_fecha.Date;

  observacion := edt_obser.Text;


  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_empleado = 0 ) and (id_almacen = 0) and (observacion = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `mov_almacen`(`fecha_mov`, `tipo`, `observaciones`, `id_almacen`, `id_empleado`) VALUES (CURDATE(),''S'',:observacion,:almacen,:empleado);';

    CONN.SQL_SELECT.ParamByName('observacion').AsString := observacion;
    CONN.SQL_SELECT.ParamByName('almacen').AsString := id_almacen.ToString;
    CONN.SQL_SELECT.ParamByName('empleado').AsString := id_empleado.ToString;


    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin

      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM mov_almacen ORDER BY id DESC LIMIT 1';
      CONN.SQL_SELECT.Open;

      id_mov := CONN.SQL_SELECT.FieldByName('id').AsInteger;

      for I := 0 to row-1 do
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM productos WHERE nombre = :name';

        CONN.SQL_SELECT.ParamByName('name').AsString := grd_salidas.Cells[0, I];
        CONN.SQL_SELECT.Open;
        id_producto := CONN.SQL_SELECT.FieldByName('id').AsInteger;

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `mov_detalle`(`cantidad`, `id_mov`, `id_producto`) VALUES (:cantidad,:id_mov,:producto);';

        CONN.SQL_SELECT.ParamByName('cantidad').AsString := grd_salidas.Cells[1, I];
        CONN.SQL_SELECT.ParamByName('producto').AsString := id_producto.ToString;
        CONN.SQL_SELECT.ParamByName('id_mov').AsString := id_mov.ToString;
        CONN.SQL_SELECT.ExecSQL;

        if (CONN.SQL_SELECT.RowsAffected > 0) then
        begin
          ShowMessage('¡Se ha registrado la información!');
        end
        else
        begin
          ShowMessage('Algo ha salido mal...');
        end;

      end;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;
end;

procedure TFrame_Salida.btn_agregarClick(Sender: TObject);
var
id_tran, I: integer;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM transacciones WHERE folio = :name LIMIT 1;';

  CONN.SQL_SELECT.ParamByName('name').AsString := ComboBox2.Selected.Text;

  CONN.SQL_SELECT.Open;

  if CONN.SQL_SELECT.RecordCount > 0 then
  begin
    id_tran := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT b.nombre AS "producto", a.cantidad  FROM transaccion_detalles a '+
  'JOIN productos b ON a.id_producto = b.id JOIN transacciones c ON a.id_transaccion = c.id WHERE c.id = :id_this;';

  CONN.SQL_SELECT.ParamByName('id_this').AsString := id_tran.ToString;

  CONN.SQL_SELECT.Open;

  if CONN.SQL_SELECT.RecordCount > 0 then
  begin
    for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
    begin

      grd_salidas.Cells[0, I] := CONN.SQL_SELECT.FieldByName('producto').AsString;
      grd_salidas.Cells[1, I] := CONN.SQL_SELECT.FieldByName('cantidad').AsString;
      row := row +1;
      CONN.SQL_SELECT.Next;
    end;
  end;

end;

procedure TFrame_Salida.btn_borrarClick(Sender: TObject);
begin
  grd_salidas.RowCount := 0;
  grd_salidas.RowCount := 100;
end;

procedure TFrame_Salida.cargardatos;
begin

  dateedt_fecha.Date := Now;
  row:= 0;

  grd_salidas.RowCount := 0;
  grd_salidas.RowCount := 100;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    ComboBox1.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT nombre FROM empleados;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    combo_client.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT folio, fecha FROM transacciones WHERE tipo = ''V'' ORDER BY fecha DESC;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    ComboBox2.Items.Add(CONN.SQL_SELECT.FieldByName('folio').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

end.
