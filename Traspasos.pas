﻿unit Traspasos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.ListBox,
  FMX.Objects, FMX.Controls.Presentation, NuevoTraspaso, DetalleTraspasos;

type
  TFrame5 = class(TFrame)
    pan_Fondo: TPanel;
    rec_Contenido: TRectangle;
    lbl_Traspasos: TLabel;
    Line1: TLine;
    lbl_Busqueda: TLabel;
    lbl_AlmOrigen: TLabel;
    cb_AlmOrigen: TComboBox;
    lbl_AlmDestino: TLabel;
    cb_AlmDestino: TComboBox;
    grd_Traspasos: TStringGrid;
    btn_NuevoTraspaso: TCornerButton;
    btn_BuscarAlm: TButton;
    Nuevo_Traspaso1: TNuevo_Traspaso;
    StringColumn6: TStringColumn;
    btn_detalles: TCornerButton;
    Frame131: TFrame13;
    {Frame71: TFrame7;}
    procedure btn_NuevoTraspasoClick(Sender: TObject);
    procedure cargartabla;

    procedure btn_BuscarAlmClick(Sender: TObject);
    procedure Nuevo_Traspaso1btn_AceptarClick(Sender: TObject);
    procedure Nuevo_Traspaso1btn_CancelarClick(Sender: TObject);
    procedure Nuevo_Traspaso1btn_BorrarNvTrasClick(Sender: TObject);
    procedure grd_TraspasosCellClick(const Column: TColumn; const Row: Integer);
    procedure btn_detallesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var id_trans : integer;
  end;

implementation
uses datamodule;

{$R *.fmx}

var id_origen : Integer;
var id_destino : Integer;
var I : Integer;

procedure TFrame5.btn_BuscarAlmClick(Sender: TObject);
begin
CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_AlmOrigen.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_origen := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_AlmDestino.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_destino := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

Datamodule.CONN.SQL_SELECT.Close;
  Datamodule.CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.fecha, a.tipo, b.nombre AS "origen", c.nombre AS "destino" ' +
                                          'FROM mov_almacen a" ' + ' JOIN almacenes b ON a.almacen_origen JOIN almacenes c ON a.almacen_destino = c.id ' +
                                           'WHERE a.id = :id_origen AND c.id = :id_destino;';
  CONN.SQL_SELECT.ParamByName('id_origen').AsString := id_origen.ToString;
  CONN.SQL_SELECT.ParamByName('id_destino').AsString := id_destino.ToString;
  CONN.SQL_SELECT.Open;

  CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.fecha_mov, a.tipo, b.nombre AS "almacen_origen", b.nombre AS "almacen_destino" FROM' + ' mov_almacen a JOIN almacenes b ON a.almacen_origen = b.id JOIN almacenes c ON a.almacen_destino = c.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_Traspasos.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          grd_Traspasos.Cells[0, I] := CONN.SQL_SELECT.FieldByName('fecha_mov').AsString;
          grd_Traspasos.Cells[1, I] := CONN.SQL_SELECT.FieldByName('tipo').AsString;
          grd_Traspasos.Cells[2, I] := CONN.SQL_SELECT.FieldByName('almacen origen').AsString;
          grd_Traspasos.Cells[3, I] := CONN.SQL_SELECT.FieldByName('almacen destino').AsString;
          CONN.SQL_SELECT.Next;
        end;

    end;
  Datamodule.CONN.SQL_SELECT.Open;


end;

procedure TFrame5.btn_detallesClick(Sender: TObject);
begin
Frame131.id_trans := self.id_trans;
Frame131.cargardatos;
frame131.Visible := True;
end;

procedure TFrame5.btn_NuevoTraspasoClick(Sender: TObject);
begin
Nuevo_Traspaso1.Visible := True;
Nuevo_Traspaso1.cargardatos;
end;



procedure TFrame5.cargartabla;

  begin

    {
      Esta función se encarga de cargar la tabla de información cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la información que mostrará
      si esta regresa más de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_AlmOrigen.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_AlmOrigen.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_AlmDestino.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_AlmDestino.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.fecha_mov,  b.nombre AS "almacen_origen", c.nombre AS "almacen_destino" FROM' + ' mov_almacen a JOIN almacenes b ON a.almacen_origen = b.id JOIN almacenes c ON a.almacen_destino = c.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_Traspasos.Cells[0, I] := CONN.SQL_SELECT.FieldByName('id').AsString;
          grd_Traspasos.Cells[1, I] := CONN.SQL_SELECT.FieldByName('fecha_mov').AsString;
          grd_Traspasos.Cells[2, I] := CONN.SQL_SELECT.FieldByName('almacen_origen').AsString;
          grd_Traspasos.Cells[3, I] := CONN.SQL_SELECT.FieldByName('almacen_destino').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;
  end;

procedure TFrame5.grd_TraspasosCellClick(const Column: TColumn;
  const Row: Integer);
begin
id_trans := strToInt(grd_Traspasos.Cells[0, row]);
btn_detalles.Enabled := True;
end;

procedure TFrame5.Nuevo_Traspaso1btn_AceptarClick(Sender: TObject);
var
id_almacenO, id_almacenD, id_mov, empleado, cantidad, id_prod: integer;
observaciones : string;
begin

   CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM empleados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := Nuevo_Traspaso1.cb_EmpleadoNvTras.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      empleado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := Nuevo_Traspaso1.cb_AlmOriNvTras.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_almacenO := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := Nuevo_Traspaso1.cb_AlmDestNvTras.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_almacenD := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM productos WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := Nuevo_Traspaso1.cb_ProduNvTras.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_prod := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    observaciones := Nuevo_Traspaso1.edt_ObservacionesNVTras.Text;

if (id_almacenO = 0 ) and (id_almacenD = 0) and (empleado = 0) and (cantidad = 0) and (id_prod = 0) then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `mov_almacen`(`fecha_mov`, `tipo`, `observaciones`, `almacen_origen`, `almacen_destino`, `id_empleado`)'
    +' VALUES (CURDATE(),''T'',:observaciones,:id_almacenO,:id_almacenD,:empleado);';

    CONN.SQL_SELECT.ParamByName('observaciones').AsString := observaciones;
    CONN.SQL_SELECT.ParamByName('id_almacenO').AsString := id_almacenO.ToString;
    CONN.SQL_SELECT.ParamByName('id_almacenD').AsString := id_almacenD.ToString;
    CONN.SQL_SELECT.ParamByName('empleado').AsString := empleado.ToString;


    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM mov_almacen ORDER BY id DESC LIMIT 1';
      CONN.SQL_SELECT.Open;

      id_mov := CONN.SQL_SELECT.FieldByName('id').AsInteger;

      for I := 0 to Nuevo_Traspaso1.row-1 do
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM productos WHERE nombre = :name';

        CONN.SQL_SELECT.ParamByName('name').AsString := Nuevo_Traspaso1.StringGrid1.Cells[0, I];
        CONN.SQL_SELECT.Open;
        id_prod := CONN.SQL_SELECT.FieldByName('id').AsInteger;

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `mov_detalle`(`cantidad`,`id_mov`,`id_producto`) '+
        'VALUES (:cantidad,:id_mov,:id_prod);';

        CONN.SQL_SELECT.ParamByName('cantidad').AsString := Nuevo_Traspaso1.StringGrid1.Cells[1, I];
        CONN.SQL_SELECT.ParamByName('id_mov').AsString := id_mov.ToString;
        CONN.SQL_SELECT.ParamByName('id_prod').AsString := id_prod.ToString;

        CONN.SQL_SELECT.ExecSQL;

        {CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'u WHERE nombre = :name';}

        if (CONN.SQL_SELECT.RowsAffected > 0) then
        begin
          Nuevo_Traspaso1.Visible:= False;
          cargartabla;
        end
        else
        begin
          ShowMessage('Algo ha salido mal...');
        end;

      end;

      //frm_Registro.Visible:= False;
      //cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
end;
end;

procedure TFrame5.Nuevo_Traspaso1btn_BorrarNvTrasClick(Sender: TObject);
begin
  Nuevo_Traspaso1.btn_BorrarNvTrasClick(Sender);

end;

procedure TFrame5.Nuevo_Traspaso1btn_CancelarClick(Sender: TObject);
begin
  Nuevo_Traspaso1.btn_CancelarClick(Sender);

end;

end.
