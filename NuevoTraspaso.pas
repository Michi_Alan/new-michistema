unit NuevoTraspaso;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.DateTimeCtrls, FMX.ListBox, FMX.Objects,
  FMX.Controls.Presentation, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox;

type
  TNuevo_Traspaso = class(TFrame)
    pan_Fondo: TPanel;
    rect_Contenido: TRectangle;
    lbl_NuevoTraspaso: TLabel;
    Line1: TLine;
    lbl_EmpleadoNvTras: TLabel;
    cb_EmpleadoNvTras: TComboBox;
    lbl_AlmOriNvTras: TLabel;
    cb_AlmOriNvTras: TComboBox;
    lbl_AlmDestNvTras: TLabel;
    cb_AlmDestNvTras: TComboBox;
    DateEdit1: TDateEdit;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    lbl_PrductoNvTras: TLabel;
    cb_ProduNvTras: TComboBox;
    btn_AgregarNvTras: TButton;
    btn_BorrarNvTras: TButton;
    lbl_CantidadNvTras: TLabel;
    lbl_OberNvTras: TLabel;
    lbl_Fecha: TLabel;
    edt_cantidadNVTras: TEdit;
    edt_ObservacionesNVTras: TEdit;
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
   
    
    procedure btn_CancelarClick(Sender: TObject);
    procedure cargardatos();
    procedure btn_AgregarNvTrasClick(Sender: TObject);
    procedure btn_BorrarNvTrasClick(Sender: TObject);
  private
    { Private declarations }
  public
  var row : Integer
    { Public declarations }
  end;

implementation
uses datamodule;

{$R *.fmx}


procedure TNuevo_Traspaso.cargardatos;
begin
row:=0;
CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM empleados;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_EmpleadoNvTras.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_EmpleadoNvTras.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_AlmOriNvTras.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_AlmOriNvTras.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_AlmDestNvTras.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_AlmDestNvTras.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM productos;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_ProduNvTras.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_ProduNvTras.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

procedure TNuevo_Traspaso.btn_AgregarNvTrasClick(Sender: TObject);
begin

  StringGrid1.Cells[0, row] := cb_ProduNvTras.Selected.Text;
  StringGrid1.Cells[1, row] := edt_cantidadNvTras.Text;

  row := row+1;
  end;


procedure TNuevo_Traspaso.btn_BorrarNvTrasClick(Sender: TObject);
begin
StringGrid1.RowCount := 0;
  StringGrid1.RowCount := 100;
  row := 0;
end;

procedure TNuevo_Traspaso.btn_CancelarClick(Sender: TObject);
begin
self.Visible := False;
StringGrid1.RowCount := 0;
  StringGrid1.RowCount := 100;
  row := 0;
end;



end.
