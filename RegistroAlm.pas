unit RegistroAlm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox;

type
  TFrame3 = class(TFrame)
    rec_container: TRectangle;
    pan_Registro: TPanel;
    lbs_Registro_Alm: TLabel;
    Line1: TLine;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_Direccion: TEdit;
    lbs_Direccion: TLabel;
    edt_Colonia: TEdit;
    lbs_Colonia: TLabel;
    lbs_Municipio: TLabel;
    lbs_Estado: TLabel;
    lbs_CP: TLabel;
    edt_CP: TEdit;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    btn_Guardar: TCornerButton;
    btn_Cancelar: TCornerButton;
    Panel1: TPanel;
    cb_pais: TComboBox;
    Label1: TLabel;
    cb_sucursal: TComboBox;
    Label2: TLabel;


    function cargardatos(nombre: string): string;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_GuardarClick(Sender: TObject);
    procedure limpiardatos;
    procedure cb_EstadosClosePopup(Sender: TObject);
    procedure cb_paisClosePopup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DataModule;

{$R *.fmx}

procedure TFrame3.limpiardatos;
begin
  edt_Nombre.Text := '';
  edt_Direccion.Text :=  '';
  edt_Colonia.Text := '';
  edt_CP.Text := '';
  cb_Estados.Items.Clear;
  cb_Municipio.Items.Clear;
end;

procedure TFrame3.btn_CancelarClick(Sender: TObject);
begin
self.Visible := False;
end;

procedure TFrame3.btn_GuardarClick(Sender: TObject);

var
Nombre, Direccion, Colonia, Municipio, Estado, CP, Pais: String;
begin

  Nombre:= edt_Nombre.Text;
  Direccion := edt_Direccion.Text;
  Colonia := edt_Colonia.Text;
  CP := edt_CP.Text;

  if (Nombre = '') or (Direccion = '') or (Colonia = '') or (Municipio = '') or (Estado = '') or (CP = '') then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
            CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT `almacenes` SET `nombre`=:Nombre,`direccion`=:Direccion,`colonia`=:Colonia';

            CONN.SQL_SELECT.ParamByName('Nombre').AsString := Nombre;
            CONN.SQL_SELECT.ParamByName('Direccion').AsString := Direccion;
            CONN.SQL_SELECT.ParamByName('Colonia').AsString := Colonia;
            CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');
              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
    end;
end;

function TFrame3.cargardatos(nombre: string): string;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT nombre FROM sucursales;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_sucursal.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;

procedure TFrame3.cb_EstadosClosePopup(Sender: TObject);
var
id_estado : integer;
begin
  {
    Este evento se encargar� se cargar la informaci�n del municipio de acuerdo
    al estado seleccionado.

    @PARAMS
      /*id = Corresponde al id del estado seleccionado m�s 1 para que corresponda
           con el correcto.

    @NOTAS
      /* Siempre iniciar limpiando el combobox, esto corresponde a la funcion
        <<Items.Clear>>
      /* Hay que bloquear el combobox con el atributo Enabled, cuando seleccionemos
         un estado este se desbloquear�

  }

  try
    cb_Municipio.Items.Clear;
    cb_Municipio.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM estados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_estados.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_estado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id_estado;
    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_Municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      cb_Municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_Municipio.Enabled := False;
  end;


end;

procedure TFrame3.cb_paisClosePopup(Sender: TObject);
var
  id_pais: integer;
begin


  try
    cb_estados.Items.Clear;
    cb_estados.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM paises WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_pais.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_pais := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.nombre FROM estados a WHERE a.pais_id = :id';
    CONN.SQL_SELECT.ParamByName('id').AsInteger := id_pais;

    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_estados.Enabled := False;
  end;

end;

end.
