unit RegistroEstado;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, DataModule,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox;

type
  TFrame_RegistroEstado = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_abrev: TEdit;
    lbs_abrev: TLabel;
    cb_pais: TComboBox;
    lbs_pais: TLabel;
    edt_clave: TEdit;
    lbs_clave: TLabel;
    procedure cargardatos;
    procedure cargardatos2;
    procedure limpiardatos;
    procedure cb_paisClosePopup(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    var
     id: integer;
  end;

implementation

{$R *.fmx}
  procedure TFrame_RegistroEstado.cargardatos;
  begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
  //Estado
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id,a.clave, a.nombre, a.abrev  FROM estados a where a.id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;



    if CONN.SQL_SELECT.RecordCount >0 then
    begin
      id:= CONN.SQL_SELECT.FieldByName('id').AsInteger;
      edt_Nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      edt_abrev.Text := CONN.SQL_SELECT.FieldByName('abrev').AsString;
      edt_clave.Text := CONN.SQL_SELECT.FieldByName('clave').AsString;

    end;



  //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.

  cb_pais.Items.Add('');
  cb_pais.ItemIndex := 0;


end;


  procedure TFrame_RegistroEstado.cb_paisClosePopup(Sender: TObject);
var
  id_pais: integer;
begin

    cb_pais.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM paises WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_pais.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_pais := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.nombre FROM paises a WHERE a.id = :id';
    CONN.SQL_SELECT.ParamByName('id').AsInteger := id_pais;

    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      CONN.SQL_SELECT.Next;
    end;

end;
procedure TFrame_RegistroEstado.limpiardatos;
begin
  //Limpiamos los edit.
  edt_Clave.Text := '';
  edt_Nombre.Text := '';
  edt_Abrev.Text := '';


  //Limpiamos los Combobox
  cb_pais.Items.Clear;
end;
procedure TFrame_RegistroEstado.cargardatos2;
  begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
  end;



end.
