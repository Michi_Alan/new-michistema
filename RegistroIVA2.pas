unit RegistroIVA2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, Datamodule;

type
  Tfrm_RegistroIva = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    edt_Pais: TEdit;
    lbs_Nombre: TLabel;
    edt_iva: TEdit;
    lbs_abrev: TLabel;
    procedure cargardatos;

  private
    { Private declarations }
  public
    { Public declarations }
    var
     id: integer;
  end;

implementation

{$R *.fmx}

procedure Tfrm_RegistroIva.cargardatos;
  begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.pais, a.porcentaje  FROM iva a where a.id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;



    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      edt_pais.Text := CONN.SQL_SELECT.FieldByName('pais').AsString;
      edt_iva.Text := CONN.SQL_SELECT.FieldByName('porcentaje').AsString;

    end;
  end;

end.
