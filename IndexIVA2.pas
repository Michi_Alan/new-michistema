unit IndexIVA2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox,
  FMX.Objects, FMX.Controls.Presentation, RegistroIVA2,datamodule;

type
  Tfrm_IndexIVA = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_agregariva: TCornerButton;
    Grd_Medidas: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    frm_RegistroIva1: Tfrm_RegistroIva;
    procedure cargartabla;
    procedure Grd_MedidasCellClick(const Column: TColumn; const Row: Integer);
    procedure Grd_MedidasCellDblClick(const Column: TColumn;
    const Row: Integer);
    procedure btn_configurarClick(Sender: TObject);
    procedure frm_Registroiva1btn_configurarClick(Sender: TObject);
    procedure frm_Registroiva1btn_desactivarClick(Sender: TObject);
    procedure btn_agregarivaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
     id: integer;
  end;

implementation

{$R *.fmx}

procedure Tfrm_IndexIVA.btn_agregarivaClick(Sender: TObject);
begin
      frm_Registroiva1.id := Self.id;
   frm_Registroiva1.cargardatos;
   frm_Registroiva1.Visible := True;
end;

procedure Tfrm_indexiva.btn_configurarClick(Sender: TObject);
begin
   frm_Registroiva1.id := Self.id;
   frm_Registroiva1.cargardatos;
   frm_Registroiva1.Visible := True;
end;

procedure Tfrm_indexiva.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.pais, a.porcentaje  FROM iva a;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Medidas.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Medidas.Cells[0, I] := CONN.SQL_SELECT.FieldByName('porcentaje').AsString;
          Grd_Medidas.Cells[1, I] := CONN.SQL_SELECT.FieldByName('pais').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;


  end;

procedure Tfrm_IndexIVA.frm_Registroiva1btn_configurarClick(Sender: TObject);
begin
 frm_Registroiva1.Visible:= False;
 self.cargartabla;
end;

procedure Tfrm_indexiva.frm_Registroiva1btn_desactivarClick(
  Sender: TObject);
var
abrev, nombre: String;
begin

  abrev := frm_Registroiva1.edt_iva.Text;
  nombre := frm_Registroiva1.edt_pais.Text;

  if (nombre = '') or (abrev = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO iva (id, pais, porcentaje) VALUES (:id,:nombre, :abrev)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('abrev').AsString := abrev;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := frm_Registroiva1.id;

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      frm_Registroiva1.Visible:= False;
      Self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;
  end;

end;

procedure Tfrm_indexiva.Grd_MedidasCellClick(const Column: TColumn;
  const Row: Integer);
begin
   id :=  Row+1;
  btn_configurar.Enabled := True;
end;

procedure Tfrm_indexiva.Grd_MedidasCellDblClick(const Column: TColumn;
  const Row: Integer);
begin
  id :=  Row+1;
  btn_configurar.Enabled := True;
end;

end.

