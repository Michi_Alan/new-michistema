unit RegistroSubCliente;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation;

type
  TFrm_RegistroSubCliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line2: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_agregar: TCornerButton;
    cb_pais: TComboBox;
    edt_direccion: TEdit;
    edt_nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Pais: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    edt_colonia: TEdit;
    Label4: TLabel;
    edt_rfc: TEdit;
    edt_cp: TEdit;
    cb_cfdi: TComboBox;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    cb_estado: TComboBox;
    cb_municipio: TComboBox;
    Label2: TLabel;
    cb_cliente: TComboBox;

    procedure limpiardatos;
    procedure cargardatos;
    procedure cb_clienteClosePopup(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
uses DataModule ;

{$R *.fmx}




procedure TFrm_RegistroSubCliente.cb_clienteClosePopup(Sender: TObject);
var
text : String;

begin
     try
    cb_municipio.Items.Clear;
    cb_municipio.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := cb_estado.Selected.Index+1;
    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      cb_municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_Municipio.Enabled := False;
  end;


end;


   procedure TFrm_RegistroSubCliente.limpiardatos;
   begin

    //Limpiamos los edit.
    edt_nombre.Text := '';
    edt_rfc.Text := '';
    edt_telefono.Text := '';
    edt_direccion.Text := '';
    edt_colonia.Text := '';
    edt_cp.Text := '';



    //Limpiamos los Combobox
    cb_cfdi.Items.Clear;
    cb_cliente.Items.Clear;
    cb_pais.Items.Clear;
    cb_estado.Items.Clear;
    cb_municipio.Items.Clear;

   end;


procedure TFrm_RegistroSubCliente.cargardatos;
begin

////////////////////////////////////////   Cargar Uso CFDI ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.Descripción AS "desc", a.c_UsoCFDI AS "CFDI" FROM f4_c_usocfdi a;';
  CONN.SQL_SELECT.Open;


  while not CONN.SQL_SELECT.Eof do
  begin
       cb_cfdi.Items.Add(CONN.SQL_SELECT.FieldByName('CFDI').AsString + ' - ' + CONN.SQL_SELECT.FieldByName('desc').AsString);
       cb_cfdi.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
       CONN.SQL_SELECT.Next;
  end;


  ////////////////////////////////////////   Cargar Cliente ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM clientes a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_cliente.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_cliente.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;



 ////////////////////////////////////////   Cargar Paises ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM paises a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Pais.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_Pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  ////////////////////////////////////////   Cargar Estados ////////////////////////////////////////////////

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM estados a WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_estado.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_estado.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;


   //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.
  cb_municipio.Items.Add('');
  cb_municipio.ItemIndex := 0;


 end;


end.
