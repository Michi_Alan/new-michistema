unit DetallesVenta;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Edit,
  FMX.DateTimeCtrls, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation, DataModule;

type
  TFrame12 = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    lbs_Nombre: TLabel;
    lbs_razon: TLabel;
    lbs_regimen: TLabel;
    lbs_RFC: TLabel;
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    Rectangle1: TRectangle;
    btn_cancelar: TCornerButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure cargardatos();
  private
    { Private declarations }
  public
    { Public declarations }
    var
    id_tran: integer;
  end;

implementation

{$R *.fmx}

procedure TFrame12.cargardatos;
var
id_this, I: integer;
begin
  //
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.fecha, a.cantidad_total, a.folio, b.nombre AS "cliente", c.porcentaje FROM transacciones a'
  +' JOIN clientes b ON a.id_cliente = b.id JOIN iva c ON a.id_iva = c.id WHERE a.id = :tran';

  CONN.SQL_SELECT.ParamByName('tran').AsString := id_tran.ToString;

  CONN.SQL_SELECT.Open;

  if CONN.SQL_SELECT.RecordCount > 0 then
  begin
    id_this := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    Label5.Text := CONN.SQL_SELECT.FieldByName('porcentaje').AsString;
    Label4.Text := CONN.SQL_SELECT.FieldByName('cliente').AsString;
    Label3.Text := CONN.SQL_SELECT.FieldByName('fecha').AsString;
    Label7.Text := CONN.SQL_SELECT.FieldByName('cantidad_total').AsString;
    Label6.Text := CONN.SQL_SELECT.FieldByName('folio').AsString;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT b.nombre AS "producto", b.precio_unico, a.costo_t, a.cantidad, a.descuento FROM transaccion_detalles a '+
  'JOIN productos b ON a.id_producto = b.id JOIN transacciones c ON a.id_transaccion = c.id WHERE c.id = :id_this;';

  CONN.SQL_SELECT.ParamByName('id_this').AsString := id_this.ToString;

  CONN.SQL_SELECT.Open;

  if CONN.SQL_SELECT.RecordCount > 0 then
  begin
  for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
  begin

    StringGrid1.Cells[0, I] := CONN.SQL_SELECT.FieldByName('producto').AsString;
    StringGrid1.Cells[1, I] := CONN.SQL_SELECT.FieldByName('cantidad').AsString;
    StringGrid1.Cells[2, I] := CONN.SQL_SELECT.FieldByName('precio_unico').AsString;
    StringGrid1.Cells[3, I] := CONN.SQL_SELECT.FieldByName('descuento').AsString;
    StringGrid1.Cells[4, I] := CONN.SQL_SELECT.FieldByName('costo_t').AsString;

  end;
  end;

end;

end.
