﻿unit PedidosVenta;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, RegistroVenta, DataModule, DetallesVenta;

type
  TFrm_IndexVenta = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_Empresa: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    Frm_Registro: TFrm_RegistroVenta;
    StringColumn6: TStringColumn;
    frm_detalles: TFrame12;
    procedure btn_agregarClick(Sender: TObject);
    procedure frm_registrobtn_agregarClick(Sender: TObject);
    procedure Frm_Registrobtn_cancelarClick(Sender: TObject);
    procedure cargartabla;
    procedure Grd_EmpresaCellClick(const Column: TColumn; const Row: Integer);
    procedure btn_desactivarClick(Sender: TObject);
    procedure fram_detallesbtn_cancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    var
    id_tran: integer;
  end;

implementation

{$R *.fmx}

procedure TFrm_IndexVenta.btn_agregarClick(Sender: TObject);
begin
  frm_registro.Visible := True;
  frm_registro.cargardatos;
end;

procedure TFrm_IndexVenta.btn_desactivarClick(Sender: TObject);
begin
  frm_detalles.Visible := True;
  frm_detalles.id_tran := id_tran;
  frm_detalles.cargardatos;

end;

procedure TFrm_IndexVenta.cargartabla;
var
I: integer;
begin
  //
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.fecha, a.cantidad_total, a.folio, b.nombre AS "cliente", c.porcentaje FROM transacciones a'
  +' JOIN clientes b ON a.id_cliente = b.id JOIN iva c ON a.id_iva = c.id ORDER BY a.fecha DESC';

  CONN.SQL_SELECT.Open;

      if CONN.SQL_SELECT.RecordCount >0 then
    begin
      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Empresa.Cells[0, I] := CONN.SQL_SELECT.FieldByName('id').AsString;
          Grd_Empresa.Cells[1, I] := CONN.SQL_SELECT.FieldByName('fecha').AsString;
          Grd_Empresa.Cells[2, I] := CONN.SQL_SELECT.FieldByName('cantidad_total').AsString;
          Grd_Empresa.Cells[3, I] := CONN.SQL_SELECT.FieldByName('folio').AsString;
          Grd_Empresa.Cells[4, I] := CONN.SQL_SELECT.FieldByName('cliente').AsString;
          Grd_Empresa.Cells[5, I] := CONN.SQL_SELECT.FieldByName('porcentaje').AsString;
          CONN.SQL_SELECT.Next;
        end;

    end;

end;

procedure TFrm_IndexVenta.fram_detallesbtn_cancelarClick(Sender: TObject);
begin
  frm_detalles.Visible := False;
  cargartabla;
end;

procedure TFrm_IndexVenta.frm_registrobtn_agregarClick(Sender: TObject);
var
  I, A, id_tran, id_cliente, id_producto, id_iva, cantidad: integer;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }
    cantidad := 0;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM clientes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_Registro.cb_clientes.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_cliente := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM iva WHERE porcentaje = :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := frm_Registro.cb_iva.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_iva := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

  //fecha := frm_registro.edt_fecha.Date;

  for A := 0 to frm_registro.row-1 do
  begin
    cantidad :=  cantidad + StrToInt(frm_registro.StringGrid1.Cells[4, A]);
  end;


  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_cliente = 0 ) and (id_iva = 0) then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transacciones`(`tipo`, `fecha`, `cantidad_total`, `folio`, `id_iva`, `id_cliente`, `id_empleado`)'
    +' VALUES (''V'',CURDATE(),:total_cantidad,:folio,:iva,:cliente,:empleado);';

    CONN.SQL_SELECT.ParamByName('iva').AsString := id_iva.ToString;
    CONN.SQL_SELECT.ParamByName('cliente').AsString := id_cliente.ToString;
    CONN.SQL_SELECT.ParamByName('empleado').AsString := '1';
    CONN.SQL_SELECT.ParamByName('total_cantidad').AsString := cantidad.ToString;
    CONN.SQL_SELECT.ParamByName('folio').AsString := 'EMP_001';

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM transacciones ORDER BY id DESC LIMIT 1';
      CONN.SQL_SELECT.Open;

      id_tran := CONN.SQL_SELECT.FieldByName('id').AsInteger;

      for I := 0 to frm_registro.row-1 do
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM productos WHERE nombre = :name';

        CONN.SQL_SELECT.ParamByName('name').AsString := frm_registro.StringGrid1.Cells[0, I];
        CONN.SQL_SELECT.Open;
        id_producto := CONN.SQL_SELECT.FieldByName('id').AsInteger;

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transaccion_detalles`(`cantidad`, `id_producto`, `id_transaccion`, `costo_t`, `descuento`) '+
        'VALUES (:cantidad,:producto,:tran,:costo,:descuento);';

        CONN.SQL_SELECT.ParamByName('cantidad').AsString := frm_registro.StringGrid1.Cells[1, I];
        CONN.SQL_SELECT.ParamByName('producto').AsString := id_producto.ToString;
        CONN.SQL_SELECT.ParamByName('tran').AsString := id_tran.ToString;
        CONN.SQL_SELECT.ParamByName('costo').AsString := frm_registro.StringGrid1.Cells[2, I];
        CONN.SQL_SELECT.ParamByName('descuento').AsString := frm_registro.StringGrid1.Cells[3, I];
        CONN.SQL_SELECT.ExecSQL;

        if (CONN.SQL_SELECT.RowsAffected > 0) then
        begin
          frm_Registro.Visible:= False;
          cargartabla;
        end
        else
        begin
          ShowMessage('Algo ha salido mal...');
        end;

      end;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;

end;

procedure TFrm_IndexVenta.Frm_Registrobtn_cancelarClick(Sender: TObject);
begin
  Frm_Registro.Visible := False;
  Frm_Registro.limpiardatos;
end;

procedure TFrm_IndexVenta.Grd_EmpresaCellClick(const Column: TColumn;
  const Row: Integer);
begin
  id_tran := StrToInt(Grd_Empresa.Cells[0, Row]);
  btn_desactivar.Enabled := True;
end;

end.
