unit RegistroGrupos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListBox, FMX.Controls.Presentation, FMX.Edit, FMX.TabControl,
  FMX.Objects, DataModule;

type
  Tregistro_grupos = class(TFrame)
    Edit1: TEdit;
    lbl_name: TLabel;
    Panel1: TPanel;
    tabs: TTabControl;
    usuarios: TTabItem;
    btn_u_add: TButton;
    btn_u_remove: TButton;
    lb_u_available: TListBox;
    lb_u_selected: TListBox;
    lbl_u_available: TLabel;
    lbl_u_selected: TLabel;
    lbs_Titulo: TLabel;
    Line1: TLine;
    btn_desactivar: TCornerButton;
    btn_configurar: TCornerButton;
    procedure loadData;
    procedure btn_u_addClick(Sender: TObject);
    procedure btn_configurarClick(Sender: TObject);
    procedure btn_u_removeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}
 procedure Tregistro_grupos.btn_configurarClick(Sender: TObject);
begin
self.Visible:=False;
end;

procedure Tregistro_grupos.btn_u_addClick(Sender: TObject);
 var i:integer;
begin

  lb_u_selected.items.AddObject(lb_u_available.Selected.ItemData.Text ,lb_u_available.Selected);
  lb_u_available.Selected.Destroy;



    //lb_u_available.Items.add
end;

procedure Tregistro_grupos.btn_u_removeClick(Sender: TObject);
begin

   lb_u_available.items.AddObject(lb_u_selected.Selected.ItemData.Text ,lb_u_available.Selected);
   lb_u_selected.Selected.Destroy;


end;

procedure Tregistro_grupos.loadData;
 var i : integer;
 begin
     CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.usuario, a.correo FROM usuarios a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          lb_u_available.items.add(CONN.SQL_SELECT.FieldByName('usuario').AsString);
          CONN.SQL_SELECT.Next;
        end;


    end;

    CONN.SQL_SELECT.Close;
 end;

end.
