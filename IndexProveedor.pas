unit IndexProveedor;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.Grid, Data.Bind.DBScope, Proveedor;

type
  TFam_indexprovedor = class(TFrame)
    index_proveedor: TRectangle;
    Rectangle1: TRectangle;
    Line1: TLine;
    Rectangle2: TRectangle;
    btn_agregar: TCornerButton;
    Label1: TLabel;
    btn_modificar: TCornerButton;
    btn_eliminar: TCornerButton;
    Grid1: TGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Frame_proveedor1: TFrame_proveedor;
 
    procedure btn_eliminarClick(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure btn_modificarClick(Sender: TObject);
    procedure Grid1CellClick(const Column: TColumn; const Row: Integer);
    procedure Frame_proveedor1btn_cancelarClick(Sender: TObject);
    procedure Grid1CellDblClick(const Column: TColumn; const Row: Integer);
    procedure Frame_proveedor1btn_actualizarClick(Sender: TObject);
    procedure Frame_proveedor1btn_guardarClick(Sender: TObject);

  private
    { Private declarations }
  public

     procedure cargar_proveedor;
      var
      id: integer;
  end;

implementation

{$R *.fmx}

uses DataModule;




procedure TFam_indexprovedor.btn_agregarClick(Sender: TObject);
begin
  Frame_proveedor1.Visible := True;
  Frame_proveedor1.cargar_paises;
  Frame_proveedor1.cargardatos_estados;
  Frame_proveedor1.cargardatos_municipio;
  Frame_proveedor1.btn_actualizar.Visible := False;
  Frame_proveedor1.btn_guardar.Visible := True;


end;

procedure TFam_indexprovedor.btn_modificarClick(Sender: TObject);
begin
  Frame_proveedor1.id := Self.id;
  Frame_proveedor1.cargardatos_proveedores;
  Frame_proveedor1.cargar_paises;
  Frame_proveedor1.cargardatos_estados;
  Frame_proveedor1.cargardatos_municipio;
  Frame_proveedor1.Visible := True;
  btn_eliminar.Enabled := False;
  btn_modificar.Enabled := False;
  btn_agregar.Enabled := True ;
  Frame_proveedor1.btn_actualizar.Visible := true;
  Frame_proveedor1.btn_guardar.Visible := False;


end;

procedure TFam_indexprovedor.btn_eliminarClick(Sender: TObject);
begin
//mensaje de eliminar
 if MessageDlg('Desea Eliminar?',TMsgDlgType.mtConfirmation, mbYesNo, 0)= 6 then

  begin
     //llamar el dataMule con el nombre conn
     CONN.Eliminar_proveedo;
     btn_eliminar.Enabled := False;
     btn_modificar.Enabled := False;
     btn_agregar.Enabled := True;
 end;


end;

//procedure TFam_indexprovedor.cargartabla_proveedor;
//
//  begin
//     CONN.UniQuery_proveedor.Close;
//
//     conn.UniQuery_proveedor.SQL.Add('SELECT p.id, p.nombre, p.rfc, p.telefono, ' +
//     'p.direccion, p.direccion, p.colonia, p.correo, p.cp, m.nombre as Municipio, ' +
//     'e.nombre as Estado, pa.nombre as Pais, p.nombre_contacto FROM proveedores as p, ' +
//     'paises as pa, estados as e, municipios as m WHERE p.id_pais = pa.id and  ' +
//     'p.id_estado = e.id and p.id_municipio = m.id  ORDER BY p.id');
//     CONN.UniQuery_proveedor.Open;
//
////    CONN.SQL_SELECT.Close;
////   CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.rfc, a.telefono, a.direccion, a.colonia, a.correo FROM proveedores a';
////
////    CONN.SQL_SELECT.Open;
////
////    if CONN.SQL_SELECT.RecordCount >0 then
////    begin
////
////      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
////        begin
////          GridProveedor.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
////          GridProveedor.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
////          GridProveedor.Cells[1, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
////          GridProveedor.Cells[2, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
////          GridProveedor.Cells[3, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
////          GridProveedor.Cells[4, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
////          GridProveedor.Cells[5, I] := CONN.SQL_SELECT.FieldByName('correo').AsString;
////          //GridProveedor.Cells[6, I] := CONN.SQL_SELECT.FieldByName('telefono').AsString;
////          CONN.SQL_SELECT.Next;
////        end;
////
////    end;
////
//
//  end;
//
//

procedure TFam_indexprovedor.cargar_proveedor;
begin
    CONN.UniQuery_proveedor.SQL.Clear;

     conn.UniQuery_proveedor.SQL.Add('SELECT p.id, p.nombre, p.rfc, p.telefono, ' +
     'p.direccion,  p.colonia, p.correo, p.cp, m.nombre as Municipio, ' +
     'e.nombre as Estado, pa.nombre as Pais, p.nombre_contacto FROM proveedores as p, ' +
     'paises as pa, estados as e, municipios as m WHERE p.id_pais = pa.id and  ' +
     'p.id_estado = e.id and p.id_municipio = m.id  ORDER BY p.id');
     CONN.UniQuery_proveedor.Open;
end;

procedure TFam_indexprovedor.Frame_proveedor1btn_actualizarClick(
  Sender: TObject);
begin
  Frame_proveedor1.btn_actualizarClick(Sender);
  cargar_proveedor;
  Frame_proveedor1.Visible := False;

end;

procedure TFam_indexprovedor.Frame_proveedor1btn_cancelarClick(Sender: TObject);
begin
  Frame_proveedor1.btn_cancelarClick(Sender);
  Frame_proveedor1.Visible := False;
  cargar_proveedor;
  Frame_proveedor1.limpiar;

end;

procedure TFam_indexprovedor.Frame_proveedor1btn_guardarClick(Sender: TObject);
begin
  Frame_proveedor1.btn_guardarClick(Sender);


end;

procedure TFam_indexprovedor.Grid1CellClick(const Column: TColumn;
  const Row: Integer);
begin
  //CONN.UniQuery_proveedor.Open;
    id :=  Row+1;

end;



procedure TFam_indexprovedor.Grid1CellDblClick(const Column: TColumn;
  const Row: Integer);
begin
 id :=  Row+1;
  btn_agregar.Enabled := False;
  btn_modificar.Enabled := True;
  btn_eliminar.Enabled := True;
end;

end.
