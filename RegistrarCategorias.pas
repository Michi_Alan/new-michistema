unit RegistrarCategorias;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, FMX.Edit, Data.DB, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TReg_categoriasProducto = class(TFrame)
    Rectangle1fondo: TRectangle;
    Rectan_cuerpo: TRectangle;
    Edit_nombre: TEdit;
    btn_cancelar_cerrar: TCornerButton;
    btn_guardar_datos: TCornerButton;
    Label1: TLabel;
    Line1: TLine;
    DataSource1: TDataSource;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkControlToField1: TLinkControlToField;
    nombre_laber: TLabel;
    procedure btn_guardar_datosClick(Sender: TObject);
    procedure btn_cancelar_cerrarClick(Sender: TObject);
    //para limpiar el campo
     procedure  limpiar_campo();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses DataModule, IndexCategoriaProducto;

procedure TReg_categoriasProducto.btn_cancelar_cerrarClick(Sender: TObject);
//Primero unir el form que queremos y tener en cuenta el nombre

begin
  // Limpiara campo;
  limpiar_campo();
end;

procedure TReg_categoriasProducto.btn_guardar_datosClick(Sender: TObject);

begin


//Validaciones de text nombre
//El Trim quita los espacios que hay ese campo
      Edit_nombre.Text := Trim(Edit_nombre.Text);
    if Length(Edit_nombre.Text)= 0 then
    begin
      ShowMessage('�Tienes que Agregar un nombre!');
       Edit_nombre.SetFocus;
       Exit;
       end
       else
       //El Teneer en cuenta el nombre que agregamos en datamodule
       Conn.Aplicar;
      begin
      ShowMessage('�Se ha Guardo la categoria!');
      limpiar_campo();
      end;

end;

procedure TReg_categoriasProducto.limpiar_campo;
begin
  //Dejar el campo basio
  Edit_nombre.Text := '';
end;

end.
