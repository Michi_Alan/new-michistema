unit DataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UniProvider, MySQLUniProvider,
  MemDS, DBAccess, Uni, Datasnap.DBClient, Datasnap.Provider;

type
  TCONN = class(TDataModule)
    SQL_CONNECTION: TUniConnection;
    SQL_SELECT: TUniQuery;
    MySQLUniProvider1: TMySQLUniProvider;
    UniQuery1: TUniQuery;
    ClientDataSet1: TClientDataSet;
    DataSetProvider1 : TDataSetProvider;
    UniQuery_proveedor: TUniQuery;
    DataSetProvider2: TDataSetProvider;
    Client_proveedor: TClientDataSet;
    DataSetProvider3: TDataSetProvider;
    UniQuery2: TUniQuery;
    Clientproductos: TClientDataSet;
    procedure Nuevo;
    procedure Aplicar;
    procedure Modificar;
    procedure Eliminar;
    procedure Mostrar_proveedor;
    procedure Eliminar_proveedo;
    procedure Nuevo_proveedo;
    procedure Eliminar_productos;



  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  CONN: TCONN;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TCONN }

procedure TCONN.Aplicar;
begin
    ClientDataSet1.Post;    //Guardar los datos en RAM
    ClientDataSet1.ApplyUpdates(0);  // pasar los datos a la base
    ClientDataSet1.Close;
    ClientDataSet1.Open;
end;



procedure TCONN.Eliminar;
begin
    ClientDataSet1.Delete;    //Guardar los datos en RAM
    ClientDataSet1.ApplyUpdates(0);  // pasar los datos a la base
    ClientDataSet1.Close;
    ClientDataSet1.Open;
end;

procedure TCONN.Eliminar_productos;
begin
    Clientproductos.Delete;    //Guardar los datos en RAM
    Clientproductos.ApplyUpdates(0);  // pasar los datos a la base
    Clientproductos.Close;
    Clientproductos.Open;
end;

procedure TCONN.Eliminar_proveedo;
begin
    Client_proveedor.Delete;    //Guardar los datos en RAM
    Client_proveedor.ApplyUpdates(0);  // pasar los datos a la base
    Client_proveedor.Close;
    Client_proveedor.Open;
end;



procedure TCONN.Modificar;
begin
    ClientDataSet1.Edit;
end;

procedure TCONN.Mostrar_proveedor;
begin
    Client_proveedor.Post;    //Guardar los datos en RAM
    Client_proveedor.ApplyUpdates(0);  // pasar los datos a la base
    Client_proveedor.Close;
    Client_proveedor.Open;
end;

procedure TCONN.Nuevo;
begin
    ClientDataSet1.Insert;
end;

procedure TCONN.Nuevo_proveedo;
begin
   Client_proveedor.Insert;
end;

end.
