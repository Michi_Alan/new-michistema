unit RegistroEmp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation, DataModule;

type
  TFrame_RegistroEmpresa = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    edt_Colonia: TEdit;
    edt_CP: TEdit;
    edt_Direccion: TEdit;
    edt_Nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Estado: TLabel;
    lbs_Municipio: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    lbs_regimen: TLabel;
    edt_rfc: TEdit;
    lbs_RFC: TLabel;
    edt_razonsocial: TEdit;
    lbs_razon: TLabel;
    cb_regimen: TComboBox;
    lbs_pais: TLabel;
    cb_pais: TComboBox;

    procedure limpiardatos;
    procedure cargardatos;
    procedure cb_EstadosClosePopup(Sender: TObject);
    procedure cb_paisClosePopup(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TFrame_RegistroEmpresa.cb_EstadosClosePopup(Sender: TObject);
var
id_estado : integer;
begin
  {
    Este evento se encargar� se cargar la informaci�n del municipio de acuerdo
    al estado seleccionado.

    @PARAMS
      /*id = Corresponde al id del estado seleccionado m�s 1 para que corresponda
           con el correcto.

    @NOTAS
      /* Siempre iniciar limpiando el combobox, esto corresponde a la funcion
        <<Items.Clear>>
      /* Hay que bloquear el combobox con el atributo Enabled, cuando seleccionemos
         un estado este se desbloquear�

  }

  try
    cb_Municipio.Items.Clear;
    cb_Municipio.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM estados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_estados.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_estado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id_estado;
    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_Municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      cb_Municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_Municipio.Enabled := False;
  end;


end;

procedure TFrame_RegistroEmpresa.cb_paisClosePopup(Sender: TObject);
var
  id_pais: integer;
begin


  try
    cb_estados.Items.Clear;
    cb_estados.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM paises WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_pais.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_pais := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.nombre FROM estados a WHERE a.pais_id = :id';
    CONN.SQL_SELECT.ParamByName('id').AsInteger := id_pais;

    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_estados.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_estados.Enabled := False;
  end;

end;

procedure TFrame_RegistroEmpresa.limpiardatos;
begin
  //Limpiamos los edit.
  edt_Colonia.Text := '';
  edt_CP.Text := '';
  edt_Direccion.Text := '';
  edt_Nombre.Text := '';
  edt_telefono.Text := '';
  edt_rfc.Text := '';
  edt_razonsocial.Text := '';

  //Limpiamos los Combobox
  cb_Estados.Items.Clear;
  cb_Municipio.Items.Clear;
  cb_Estados.Items.Clear;
end;


procedure TFrame_RegistroEmpresa.cargardatos;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM paises;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.Descripci�n AS "desc", a.c_RegimenFiscal AS "RF" FROM f4_c_regimenfiscal a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_regimen.Items.Add(CONN.SQL_SELECT.FieldByName('RF').AsString +' - '+ CONN.SQL_SELECT.FieldByName('desc').AsString);
    cb_regimen.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

  //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.

  cb_Municipio.Items.Add('');
  cb_Municipio.ItemIndex := 0;


end;

end.
