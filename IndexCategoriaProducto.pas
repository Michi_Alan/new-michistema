unit IndexCategoriaProducto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, System.Rtti, FMX.Grid.Style, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Grid, Data.Bind.Components, Data.Bind.DBScope, Data.DB,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Grid, RegistrarCategorias;

type
  TIndex_CategoriaProductos = class(TFrame)
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    Reg_categoriasProducto1: TReg_categoriasProducto;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    StringGrid1: TStringGrid;
    btn_guardar: TCornerButton;
    btn_modificar: TCornerButton;
    btn_eliminar: TCornerButton;
    Label2: TLabel;
    Line1: TLine;
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_modificarClick(Sender: TObject);
    procedure btn_eliminarClick(Sender: TObject);
    procedure Reg_categoriasProducto1btn_cancelar_cerrarClick(Sender: TObject);
    procedure Reg_categoriasProducto1btn_guardar_datosClick(Sender: TObject);
    procedure FrameClick(Sender: TObject);
    procedure StringGrid1CellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses DataModule;

procedure TIndex_CategoriaProductos.btn_eliminarClick(Sender: TObject);
//Primero unir el form que queremos y tener en cuenta el nombre
begin
//mensaje de eliminar
 if MessageDlg('Desea Eliminar?',TMsgDlgType.mtConfirmation, mbYesNo, 0)= 6 then

 begin
     //llamar el dataMule con el nombre conn
     CONN.Eliminar;
     btn_eliminar.Enabled := False;
     btn_modificar.Enabled := False;
     btn_guardar.Enabled := True ;
 end;



end;

procedure TIndex_CategoriaProductos.btn_guardarClick(Sender: TObject);

//Primero unir el form que queremos y tener en cuenta el nombre

begin
    //llamar el dataMule con el nombre conn
     CONN.Nuevo;

     Reg_categoriasProducto1.Visible := True;
end;

procedure TIndex_CategoriaProductos.btn_modificarClick(Sender: TObject);
//Primero unir el form que queremos y tener en cuenta el nombre
begin
     CONN.Modificar;
     //Para abrir el formulario de registro
     //Reg_categoriasProducto1.Create(Application);
     Reg_categoriasProducto1.Visible := True;
     btn_eliminar.Enabled := False;
     btn_modificar.Enabled := False;
     btn_guardar.Enabled := True ;
end;

procedure TIndex_CategoriaProductos.FrameClick(Sender: TObject);
begin
////no podra modidicar antes de que seleciona un id
//btn_modificar.Enabled := False;
//btn_eliminar.Enabled := False;
//btn_guardar.Enabled := True;
end;
 //Cancelar
procedure TIndex_CategoriaProductos.Reg_categoriasProducto1btn_cancelar_cerrarClick(
  Sender: TObject);
begin
  Reg_categoriasProducto1.btn_cancelar_cerrarClick(Sender);

  Reg_categoriasProducto1.Visible := False;

end;

//Guardar
procedure TIndex_CategoriaProductos.Reg_categoriasProducto1btn_guardar_datosClick(
  Sender: TObject);
begin
  Reg_categoriasProducto1.btn_guardar_datosClick(Sender);
  Reg_categoriasProducto1.Visible := False;
  Reg_categoriasProducto1.limpiar_campo;

end;

procedure TIndex_CategoriaProductos.StringGrid1CellClick(const Column: TColumn;
  const Row: Integer);
begin
//Para habilitar el boton
     btn_eliminar.Enabled := True;
     btn_modificar.Enabled := True;
     btn_guardar.Enabled := False ;
end;

end.
