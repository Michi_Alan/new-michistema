unit PermisosUsuarios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, FMX.Objects, RegistroSuc,
  DataModule,FMX.Controls.Presentation, FMX.ListBox;

type
  Tfrm_permisosUsusarios = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    serv_edit: TCheckBox;
    alm_edit: TCheckBox;
    trans_edit: TCheckBox;
    cb_usuario: TComboBox;
    btn_guardar: TCornerButton;
    btn_cancelar: TCornerButton;
    conf_edit: TCheckBox;
    procedure cargarusuarios;
    procedure cargarpermisos;
    procedure cb_usuarioClosePopup(Sender: TObject);
    procedure btn_agregarClick(Sender: TObject);
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}



procedure Tfrm_permisosUsusarios.btn_agregarClick(Sender: TObject);
begin
 cargarusuarios;
end;

procedure Tfrm_permisosUsusarios.btn_cancelarClick(Sender: TObject);
begin
self.Visible:=False;
end;

procedure Tfrm_permisosUsusarios.btn_guardarClick(Sender: TObject);
var perm:integer;
  I: Integer;
begin

end;

procedure Tfrm_permisosUsusarios.cargarusuarios;
begin
  CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id, usuario FROM usuarios order by id asc';
    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_usuario.Items.Add(CONN.SQL_SELECT.FieldByName('usuario').AsString);
      cb_usuario.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
      CONN.SQL_SELECT.Next;
    end;
end;
procedure Tfrm_permisosUsusarios.cargarpermisos;
var conf, servicios, almacen, transacciones:boolean;
const
idRC = 1;
idRS = 2;
idRA = 3;
idRT = 4;
begin
    //Check rol conf
     CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT  id_rol FROM roles_usuarios where id_usuario=:idU AND id_rol=:idRC';
    CONN.SQL_Select.ParamByName('idU').AsInteger := cb_usuario.Selected.Index;
    CONN.SQL_Select.ParamByName('idRC').AsInteger := idRC;
    showmessage(cb_usuario.Index.ToString);
    showmessage(idRC.ToString);
    CONN.SQL_SELECT.Open;
    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      conf := True;
      conf_edit.IsChecked := True;
      showmessage('conf');
    end
    else
    begin
      conf := False;
      conf_edit.IsChecked := False;
    end;
    //SERVICIOS
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT  id_rol FROM roles_usuarios where id_usuario=:idU AND id_rol=:idRC';
    CONN.SQL_Select.ParamByName('idU').AsInteger := cb_usuario.Index;
    CONN.SQL_Select.ParamByName('idRC').AsInteger := idRS;
    CONN.SQL_SELECT.Open;
    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      serv_edit.IsChecked := True;
      servicios := True;
    end
    else
    begin
      serv_edit.IsChecked := False;
      servicios := False;
    end;
    //ALMACEN
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT  id_rol FROM roles_usuarios where id_usuario=:idU AND id_rol=:idRC';
    CONN.SQL_Select.ParamByName('idU').AsInteger := cb_usuario.Index;
    CONN.SQL_Select.ParamByName('idRC').AsInteger := idRA;
    CONN.SQL_SELECT.Open;
    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      alm_edit.IsChecked := True;
      almacen := True;
    end
    else
    begin
      alm_edit.IsChecked :=False;
      almacen := False;
    end;
    //TRANSACCIONES
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT  id_rol FROM roles_usuarios where id_usuario=:idU AND id_rol=:idRC';
    CONN.SQL_Select.ParamByName('idU').AsInteger := cb_usuario.Index;
    CONN.SQL_Select.ParamByName('idRC').AsInteger := idRT;
    CONN.SQL_SELECT.Open;
    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
    trans_edit.IsChecked :=True;
      transacciones := True;
    end
    else
    begin
    trans_edit.IsChecked :=FaLSE;
      transacciones := False;
    end;

end;


procedure Tfrm_permisosUsusarios.cb_usuarioClosePopup(Sender: TObject);
begin
          cargarpermisos;
end;

end.
