﻿unit PedidosCotizacion;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, RegistroCotizacion;

type
  TFrm_PedidosCotizacion = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_Empresa: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    StringColumn7: TStringColumn;
    StringColumn8: TStringColumn;
    StringColumn9: TStringColumn;
    StringColumn10: TStringColumn;
    FrmRegistroCotizacion1: TFrmRegistroCotizacion;
    procedure btn_agregarClick(Sender: TObject);
    procedure FrmRegistroCotizacion1btn_agregarClick(Sender: TObject);
    procedure FrmRegistroCotizacion1btn_cancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
uses DataModule ;
{$R *.fmx}



procedure TFrm_PedidosCotizacion.btn_agregarClick(Sender: TObject);
begin
  FrmRegistroCotizacion1.Visible := True;
  FrmRegistroCotizacion1.cargardatos;
end;





procedure TFrm_PedidosCotizacion.FrmRegistroCotizacion1btn_agregarClick(
  Sender: TObject);
var
  I, A, id_tran, id_cliente, id_producto, id_iva, cantidad: integer;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }
    cantidad := 0;
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM clientes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := FrmRegistroCotizacion1.cb_clientes.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_cliente := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM iva WHERE porcentaje = :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := FrmRegistroCotizacion1.cb_iva.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_iva := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

  //fecha := frm_registro.edt_fecha.Date;

  for A := 0 to FrmRegistroCotizacion1.row-1 do
  begin
    cantidad :=  cantidad + StrToInt(FrmRegistroCotizacion1.StringGrid1.Cells[4, A]);
  end;


  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_cliente = 0 ) and (id_iva = 0) then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transacciones`(`tipo`, `fecha`, `cantidad_total`, `folio`, `id_iva`, `id_cliente`, `id_empleado`)'
    +' VALUES (''k'',CURDATE(),:total_cantidad,:folio,:iva,:cliente,:empleado);';

    CONN.SQL_SELECT.ParamByName('iva').AsString := id_iva.ToString;
    CONN.SQL_SELECT.ParamByName('cliente').AsString := id_cliente.ToString;
    CONN.SQL_SELECT.ParamByName('empleado').AsString := '1';
    CONN.SQL_SELECT.ParamByName('total_cantidad').AsString := cantidad.ToString;
    CONN.SQL_SELECT.ParamByName('folio').AsString := 'EMP_001';

    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM transacciones ORDER BY id DESC LIMIT 1';
      CONN.SQL_SELECT.Open;

      id_tran := CONN.SQL_SELECT.FieldByName('id').AsInteger;

      for I := 0 to FrmRegistroCotizacion1.row-1 do
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM productos WHERE nombre = :name';

        CONN.SQL_SELECT.ParamByName('name').AsString := FrmRegistroCotizacion1.StringGrid1.Cells[0, I];
        CONN.SQL_SELECT.Open;
        id_producto := CONN.SQL_SELECT.FieldByName('id').AsInteger;

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transaccion_detalles`(`cantidad`, `id_producto`, `id_transaccion`, `costo_t`, `descuento`) '+
        'VALUES (:cantidad,:producto,:tran,:costo,:descuento);';

        CONN.SQL_SELECT.ParamByName('cantidad').AsString := FrmRegistroCotizacion1.StringGrid1.Cells[1, I];
        CONN.SQL_SELECT.ParamByName('producto').AsString := id_producto.ToString;
        CONN.SQL_SELECT.ParamByName('tran').AsString := id_tran.ToString;
        CONN.SQL_SELECT.ParamByName('costo').AsString := FrmRegistroCotizacion1.StringGrid1.Cells[2, I];
        CONN.SQL_SELECT.ParamByName('descuento').AsString := FrmRegistroCotizacion1.StringGrid1.Cells[3, I];
        CONN.SQL_SELECT.ExecSQL;

        if (CONN.SQL_SELECT.RowsAffected > 0) then
        begin
          FrmRegistroCotizacion1.Visible:= False;
          //cargartabla;
        end
        else
        begin
          ShowMessage('Algo ha salido mal...');
        end;

      end;

      //frm_Registro.Visible:= False;
      //cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;

  end;
end;
procedure TFrm_PedidosCotizacion.FrmRegistroCotizacion1btn_cancelarClick(
  Sender: TObject);
begin
 FrmRegistroCotizacion1.Visible := False;
 FrmRegistroCotizacion1.limpiardatos;
end;

end.
