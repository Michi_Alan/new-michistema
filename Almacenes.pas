unit Almacenes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox, FMX.Edit, RegistroAlm, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope;

type
  TFrame2 = class(TFrame)
    rec_Bg_Container: TRectangle;
    �: TPanel;
    Line1: TLine;
    lbs_Titulo_Almacen: TLabel;
    btn_nuevo: TCornerButton;
    edt_almbuscar: TEdit;
    lbs_busqueda: TLabel;
    btn_buscar: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    RegistroAlm: TFrame3;
    grd_Almacen: TStringGrid;
    btn_aceptar: TSpeedButton;
    procedure btn_nuevoClick(Sender: TObject);
    procedure btn_buscarClick(Sender: TObject);
    procedure RegistroAlmbtn_CancelarClick(Sender: TObject);
    procedure RegistroAlmbtn_GuardarClick(Sender: TObject);
    procedure cargartabla;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

uses Datamodule;
procedure TFrame2.btn_buscarClick(Sender: TObject);
begin
  Datamodule.CONN.SQL_SELECT.Close;
  Datamodule.CONN.SQL_SELECT.SQL.Text := 'SELECT ID,nombre,id_empresa,id_municipio ' +
                                          'FROM "almacenes" ' +
                                           'WHERE nombre = ''' + edt_almbuscar.Text + '''' ;
  Datamodule.CONN.SQL_SELECT.Open;
end;

procedure TFrame2.btn_nuevoClick(Sender: TObject);
begin
RegistroAlm.Visible := True;
RegistroAlm.cargardatos('123');
end;

procedure TFrame2.RegistroAlmbtn_CancelarClick(Sender: TObject);
begin
  RegistroAlm.Visible:= False;
  RegistroAlm.limpiardatos;
  self.cargartabla;
end;

procedure TFrame2.RegistroAlmbtn_GuardarClick(Sender: TObject);
var
  id_estado, id_municipio, id_sucursal, id_pais: integer;
  nombre, direc, col, cp: String;
begin
  {
    Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
    si este fuera el caso podra la variable como vacia y no podr� pasar la
    siguiente condici�n.
  }

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM paises WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := RegistroAlm.cb_pais.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_pais := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM sucursales WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := RegistroAlm.cb_sucursal.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_sucursal := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM estados WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := RegistroAlm.cb_estados.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_estado := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM municipios WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := RegistroAlm.cb_Municipio.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      id_municipio := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    nombre := RegistroAlm.edt_Nombre.Text;
    direc := RegistroAlm.edt_Direccion.Text;
    col :=  RegistroAlm.edt_Colonia.text;
    cp := RegistroAlm.edt_CP.Text;

  //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

  if (id_estado = 0 ) and (id_municipio = 0) and (nombre = '') or (col = '') or (cp = '') or
      (direc = '') then
  begin
    ShowMessage('�Los campos no pueden estar vacios!');
  end
  else
  begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO almacenes (nombre, direccion, colonia, CP, id_sucursal, id_municipio, id_estado, id_pais) VALUES (:nombre, :direc, :col, :cp, :sucursal, :muni, :estado, :pais)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('direc').AsString := direc;
    CONN.SQL_SELECT.ParamByName('col').AsString := col;
    CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
    CONN.SQL_SELECT.ParamByName('sucursal').AsString := id_sucursal.ToString;
    CONN.SQL_SELECT.ParamByName('muni').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
    CONN.SQL_SELECT.ExecSQL;

    if (CONN.SQL_SELECT.RowsAffected > 0) then
    begin
      ShowMessage('�Se ha registrado la informaci�n!');
      RegistroAlm.Visible:= False;
      self.cargartabla;
    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;


  end;
end;

procedure TFrame2.cargartabla;
var I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.direccion, a.colonia, a.CP FROM almacenes a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_Almacen.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          grd_Almacen.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
          grd_Almacen.Cells[1, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
          grd_Almacen.Cells[2, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
          grd_Almacen.Cells[3, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          CONN.SQL_SELECT.Next;
        end;

    end;
  end;

end.
