unit OrdenDeCompra;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ListBox, FMX.Objects, FMX.Edit, FMX.Controls.Presentation, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, DataModule;

type
  TOrdenCompra = class(TFrame)
    Fondo: TRectangle;
    contenido1: TRectangle;
    contenido: TRectangle;
    Lab_Formu: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit_nota: TEdit;
    Rectangle1: TRectangle;
    btn_cancelar: TCornerButton;
    btn_guardar: TCornerButton;
    Label12: TLabel;
    Line1: TLine;
    Rectangle2: TRectangle;
    Label5: TLabel;
    Edit: TLabel;
    suma_total: TLabel;
    Label2: TLabel;
    cb_iva: TComboBox;
    Label1: TLabel;
    Grd_Orden: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    Button1: TButton;
    StringColumn6: TStringColumn;
    Label4: TLabel;
    cb_proveedor: TComboBox;
    lb_folio: TLabel;
    Edit4: TEdit;
    cb_articulo: TComboBox;
    Edit2: TLabel;
    Edit1: TLabel;
    Edit3: TEdit;
    SumaSubtotal: TLabel;
    desc_tot: TEdit;
    id_prod: TLabel;
    observacion: TEdit;
    observaciones: TLabel;
    id_trans: TLabel;
    subtotal1: TLabel;
    Label9: TLabel;
    id_emp: TLabel;
    procedure cargardatos;
    procedure cargartabla;
    procedure limpiardatos;
    procedure Button1Click(Sender: TObject);
    procedure cb_proveedorExit(Sender: TObject);
    procedure cb_articuloClosePopup(Sender: TObject);
    procedure Edit3Exit(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);
    procedure Edit4Exit(Sender: TObject);
    procedure cb_ivaClosePopup(Sender: TObject);
    procedure desc_totExit(Sender: TObject);
    procedure btn_guardarClick(Sender: TObject);

  private

  public
    { Public declarations }
  end;

implementation
    var I:integer;
    ban:integer=0;
    x:integer=0;
{$R *.fmx}

procedure TOrdenCompra.btn_cancelarClick(Sender: TObject);
begin
            CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'update transacciones set folio=:folio where id='+id_trans.Text+';';
            CONN.SQL_SELECT.ParamByName('folio').AsString := 'N/A';
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');
                limpiardatos;

              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
end;

procedure TOrdenCompra.btn_guardarClick(Sender: TObject);
begin


            CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'UPDATE transacciones SET fecha=CURRENT_DATE,cantidad_total='+suma_total.Text+',descuento='+desc_tot.text+',folio=:folio,notas=:nota,id_iva=:iva, id_proveedor=:prob, id_empleado='+id_emp.text+' WHERE id = :id_trans;';
            CONN.SQL_SELECT.ParamByName('nota').AsString := Edit_nota.text;
            CONN.SQL_SELECT.ParamByName('folio').AsString := lb_folio.text;
            CONN.SQL_SELECT.ParamByName('id_trans').AsInteger := strtoint(id_trans.text);
            CONN.SQL_SELECT.ParamByName('iva').AsInteger :=  cb_iva.ItemIndex+1;
            CONN.SQL_SELECT.ParamByName('prob').AsInteger := cb_proveedor.ItemIndex+1;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');


              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
end;

procedure TOrdenCompra.Button1Click(Sender: TObject);

begin


           CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transaccion_detalles`(`cantidad`, `id_producto`, `id_transaccion`, `descripcion`, `descuento`, `subtotal`) VALUES (:cant,:prod,:trans,:obs,:desc,:sub)';

            CONN.SQL_SELECT.ParamByName('cant').AsInteger := strtoint(Edit4.Text);
            CONN.SQL_SELECT.ParamByName('prod').AsInteger := strtoint(id_prod.Text);
            CONN.SQL_SELECT.ParamByName('trans').AsInteger := strtoint(id_trans.Text);
            CONN.SQL_SELECT.ParamByName('obs').AsString := observacion.Text;
            CONN.SQL_SELECT.ParamByName('desc').AsInteger := strtoint(Edit3.Text);
            CONN.SQL_SELECT.ParamByName('sub').AsInteger := strtoint(subtotal1.Text);
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');

                cargartabla;


              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;



end;


procedure TOrdenCompra.limpiardatos;
begin
  //Limpiamos los edit.
  Edit_nota.Text := '';
  Edit1.Text := '-';
  Edit2.Text := '-';
  Edit4.Text := '';
  desc_tot.Text :='';
  suma_total.Text := '-';
  lb_folio.Text := '-';
  observacion.Text:='';
  subtotal1.Text:='-';
  id_trans.Text:='-';
  id_prod.Text:='-';

  //Limpiamos los Combobox
  cb_proveedor.Items.Clear;
  cb_iva.Items.Clear;
  cb_articulo.Items.Clear;

end;
procedure TOrdenCompra.cargardatos;
begin
                //ID_TRANS
                CONN.SQL_SELECT.Close;
                CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM `transacciones` WHERE folio=:folio';
                CONN.SQL_SELECT.Open;
                CONN.SQL_SELECT.ParamByName('folio').AsString := 'd11';
                CONN.SQL_SELECT.ExecSQL;

                id_trans.Text := inttostr(CONN.SQL_SELECT.FieldByName('id').AsInteger);
                lb_folio.Text:='C'+id_trans.text;
                //select proveedor
                CONN.SQL_SELECT.Close;
                CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM proveedores WHERE activo = 1';
                CONN.SQL_SELECT.Open;

                while not CONN.SQL_SELECT.Eof do
                begin
                  cb_proveedor.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
                  cb_proveedor.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
                  CONN.SQL_SELECT.Next;
                end;
                //select IVA
                 CONN.SQL_SELECT.Close;
                CONN.SQL_SELECT.SQL.Text := 'SELECT id, porcentaje FROM iva';
                CONN.SQL_SELECT.Open;

                while not CONN.SQL_SELECT.Eof do
                begin
                  cb_iva.Items.Add(CONN.SQL_SELECT.FieldByName('porcentaje').AsString);
                  cb_iva.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
                  CONN.SQL_SELECT.Next;
                end;


end;

procedure TOrdenCompra.cargartabla;
  var
    I: integer;
  begin



    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }           //Select en grid


    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, b.codigo, b.descripcion, a.cantidad, b.precio_unico, a.descuento, a.subtotal  FROM transaccion_detalles a JOIN productos b ON b.id=a.id_producto WHERE a.id_transaccion='+id_trans.Text+';';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Orden.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          Grd_Orden.Cells[0, I] := CONN.SQL_SELECT.FieldByName('codigo').AsString;
          Grd_Orden.Cells[1, I] := CONN.SQL_SELECT.FieldByName('descripcion').AsString;
          Grd_Orden.Cells[2, I] := CONN.SQL_SELECT.FieldByName('precio_unico').AsString;
          Grd_Orden.Cells[3, I] := CONN.SQL_SELECT.FieldByName('cantidad').AsString;
          Grd_Orden.Cells[4, I] := CONN.SQL_SELECT.FieldByName('descuento').AsString;
          Grd_Orden.Cells[5, I] := CONN.SQL_SELECT.FieldByName('subtotal').AsString;

          CONN.SQL_SELECT.Next;

        end;
    end;
         I:=0;

     while ban=0 do
        begin
          if Grd_Orden.Cells[5, I+1] = '' then ban:=1;
          x:=x+strtoint(Grd_Orden.Cells[5, I]);
          SumaSubTotal.text:=inttostr(x);
          I:=I+1;
          showmessage(inttostr(x));
        end;
    ban:=0;
    x:=0;




  end;




procedure TOrdenCompra.cb_articuloClosePopup(Sender: TObject);
begin
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id, codigo, precio_unico FROM productos WHERE id='+inttostr(cb_articulo.ItemIndex+1)+';';
      CONN.SQL_SELECT.Open;
      id_prod.Text:= CONN.SQL_SELECT.FieldByName('id').AsString;
      Edit1.Text := CONN.SQL_SELECT.FieldByName('codigo').AsString;
      Edit2.Text := CONN.SQL_SELECT.FieldByName('precio_unico').AsString;
      CONN.SQL_SELECT.ExecSQL;


end;

procedure TOrdenCompra.cb_ivaClosePopup(Sender: TObject);
var subt:double;
    descT:double;
    sum:double;
    iva:double;
    Total:double;
begin

    subt:=strtoint(SumaSubTotal.Text);
    descT:=strtoint(desc_tot.Text);
    iva:=strtoint(cb_iva.Selected.Text);
    sum:=(subt-descT);
    Total:=iva/100*sum+sum;
    suma_total.Text:=floattostr(Total);


end;

procedure TOrdenCompra.cb_proveedorExit(Sender: TObject);
begin
      cb_articulo.Items.Clear;
      //select artpiculo
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM productos WHERE id_proveedor='+inttostr(cb_proveedor.ItemIndex+1)+';';
      CONN.SQL_SELECT.Open;
      //showmessage('SELECT id, nombre FROM productos WHERE id_proveedor='+inttostr(cb_proveedor.ItemIndex+1)+';');

      while not CONN.SQL_SELECT.Eof do
      begin
        cb_articulo.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
        cb_articulo.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
        CONN.SQL_SELECT.Next;
      end;



end;

procedure TOrdenCompra.desc_totExit(Sender: TObject);
var desc:integer;
    cantidad:integer;
    precio:integer;
begin
    desc:=strtoint(Edit3.Text);
    cantidad:=strtoint(Edit4.Text);
    precio:=strtoint(Edit2.Text);
    subtotal1.Text:=inttostr((cantidad*precio)-desc);

end;

procedure TOrdenCompra.Edit3Exit(Sender: TObject);
var desc:integer;
    cantidad:integer;
    precio:integer;
begin
    desc:=strtoint(Edit3.Text);
    cantidad:=strtoint(Edit4.Text);
    precio:=strtoint(Edit2.Text);
    subtotal1.Text:=inttostr((cantidad*precio)-desc);

end;

procedure TOrdenCompra.Edit4Exit(Sender: TObject);
var desc:integer;
    cantidad:integer;
    precio:integer;
begin
    desc:=strtoint(Edit3.Text);
    cantidad:=strtoint(Edit4.Text);
    precio:=strtoint(Edit2.Text);
    subtotal1.Text:=inttostr((cantidad*precio)-desc);
end;

end.
