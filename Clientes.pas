unit Clientes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox, FMX.Edit, RegistroAlm, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, Insert_cte_shido;
type
  TFrame_IndexCliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_Empresa: TStringGrid;
    Frame_Cliente1: TFrame_Cliente;
    col2: TStringColumn;
    col9: TStringColumn;
    col5: TStringColumn;
    col6: TStringColumn;
    col4: TStringColumn;
    col12: TStringColumn;
    col10: TStringColumn;
    col11: TStringColumn;
    col13: TStringColumn;
    col14: TStringColumn;
    col15: TStringColumn;
    col7: TStringColumn;
    col8: TStringColumn;
    col3: TStringColumn;
    col1: TStringColumn;

    procedure cargartabla;
    procedure btn_agregarClick(Sender: TObject);
    procedure Frame_Cliente1btn_agregarClick(Sender: TObject);
  //  procedure Grd_EmpresaCellClick(const Column: TColumn; const Row: Integer);

  private
    { Private declarations }
  public
    { Public declarations }
  end;
  var
    I: integer;

implementation

{$R *.fmx}

uses Datamodule;

  procedure TFrame_IndexCliente.btn_agregarClick(Sender: TObject);
    begin
      frame_Cliente1.Visible := True;
     // frame_Cliente1.limpiardatos;
      frame_Cliente1.cargardatos;

     // frame_Cliente1.cargardatos_municipio;
    end;






  procedure TFrame_IndexCliente.cargartabla;



  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */
         /* ia stoi artooo alv*/
    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS ''ID'', '+
        'a.nombre AS ''Nombre'',                        '+
        'a.correo AS ''Correo'',                        '+
        'a.telefono AS ''Telefono'',                    '+
        'a.RFC AS ''RFC'',                              '+
        'a.persona_tipo AS ''Tipo_Persona'',            '+
				'd.tipo_cliente AS ''Tipo'',                    '+
				'e.nombre AS ''Empresa'',                       '+
				'f.c_UsoCFDI AS ''CFDI'',                       '+
				'a.colonia AS ''Colonia'',                      '+
				'a.cp AS ''CP'',                                '+
				'a.direccion AS ''Direccion'',                  '+
        'b.nombre as ''Pais'',                          '+
        'c.nombre as ''Estado'',                        '+
        'i.nombre AS ''Municipio''                      '+
        '                                             '+
        'FROM clientes a                              '+
        'JOIN paises b on a.id_pais = b.id            '+
        'JOIN estados c on a.id_estado = c.id         '+
        'JOIN tipo_cliente d on a.id_tipo = d.id      '+
	      'JOIN empresa e on a.id_empresa = e.id        '+
	      'JOIN f4_c_usocfdi f on a.id_cfdi = f.id      '+
        'JOIN municipios i on a.id_municipio = i.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

       for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Empresa.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
           Grd_Empresa.Cells[0, I] := CONN.SQL_SELECT.FieldByName('ID').AsString;
          Grd_Empresa.Cells[1, I] := CONN.SQL_SELECT.FieldByName('Nombre').AsString;
          Grd_Empresa.Cells[2, I] := CONN.SQL_SELECT.FieldByName('Correo').AsString;
          Grd_Empresa.Cells[3, I] := CONN.SQL_SELECT.FieldByName('Telefono').AsString;
          Grd_Empresa.Cells[4, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
          Grd_Empresa.Cells[5, I] := CONN.SQL_SELECT.FieldByName('Tipo_Persona').AsString;
          Grd_Empresa.Cells[6, I] := CONN.SQL_SELECT.FieldByName('Tipo').AsString;
          Grd_Empresa.Cells[7, I] := CONN.SQL_SELECT.FieldByName('Empresa').AsString;
          Grd_Empresa.Cells[8, I] := CONN.SQL_SELECT.FieldByName('CFDI').AsString;
          Grd_Empresa.Cells[9, I] := CONN.SQL_SELECT.FieldByName('Colonia').AsString;
          Grd_Empresa.Cells[10, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          Grd_Empresa.Cells[11,I] := CONN.SQL_SELECT.FieldByName('Direccion').AsString;
          Grd_Empresa.Cells[12, I] := CONN.SQL_SELECT.FieldByName('Pais').AsString;
          Grd_Empresa.Cells[13, I] := CONN.SQL_SELECT.FieldByName('Estado').AsString;
          Grd_Empresa.Cells[14, I] := CONN.SQL_SELECT.FieldByName('Municipio').AsString;


        CONN.SQL_SELECT.Next;
        end;
    end;


  end;






  procedure TFrame_IndexCliente.Frame_Cliente1btn_agregarClick(Sender: TObject);
   var
      personatipo, idtipo, empresa, cfdi, id_pais, id_estado, id_municipio  : integer;


      nombre, correo, telefono, rfc, colonia, cp, direccion: String;


 begin
         {
        Estas condiciones se encargaran de detectar si los combo box no han sido seleccionados
        si este fuera el caso podra la variable como vacia y no podr� pasar la
        siguiente condici�n.
      }


        personatipo := frame_Cliente1.cb_personatipo.Selected.Index+1;
        idtipo := frame_Cliente1.cb_idtipo.Selected.Index+1;
         empresa := frame_Cliente1.cb_empresa.Selected.Index+1;
         cfdi := frame_Cliente1.cb_cfdi.Selected.Index+1;
         id_pais := frame_Cliente1.cb_pais.Selected.Index+1;
        id_estado := frame_Cliente1.cb_estado.Selected.Index+1;
        id_municipio := frame_Cliente1.cb_Municipio.Selected.Index+1;

        //////////////////////////////////////////////////////////////////////
        nombre := frame_Cliente1.edt_Nombre.Text;
        correo := frame_Cliente1.edt_correo.Text;
        telefono := frame_Cliente1.edt_telefono.Text;
        rfc := frame_Cliente1.edt_rfc.Text;
        colonia :=  frame_Cliente1.edt_colonia.text;
        cp := frame_Cliente1.edt_CP.Text;
        direccion := frame_Cliente1.edt_direccion.Text;

      ShowMessage(id_estado.ToString+', '+id_municipio.ToString+', '+id_pais.ToString);

      //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

      if (personatipo = 0 ) and (idtipo = 0) and (empresa = 0) and (cfdi = 0) and (id_pais = 0)  and (id_estado = 0)and (id_municipio = 0)
       or (nombre = '')  or (correo = '') or (telefono = '') or (rfc = '') or
          (colonia = '') or (cp = '') or (direccion = '') then
      begin
        ShowMessage('�Los campos no pueden estar vacios!');
      end
      else
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO clientes '+
        '(nombre, correo, telefono, RFC, persona_tipo, id_tipo, id_empresa, id_cfdi, colonia, cp, direccion, id_pais, id_estado, id_municipio) '+
        'VALUES (:nombre, :correo, :telefono, :RFC, :persona_tipo, :id_tipo, :id_empresa, :id_cfdi, :colonia, :cp, :direccion, :id_pais, :id_estado, :id_municipio)';

        CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
        CONN.SQL_SELECT.ParamByName('correo').AsString := correo;
        CONN.SQL_SELECT.ParamByName('telefono').AsString := telefono;
        CONN.SQL_SELECT.ParamByName('RFC').AsString := RFC;
        CONN.SQL_SELECT.ParamByName('persona_tipo').AsString := personatipo.ToString;
        CONN.SQL_SELECT.ParamByName('id_tipo').AsString := idtipo.ToString;
        CONN.SQL_SELECT.ParamByName('id_empresa').AsString := empresa.ToString;
        CONN.SQL_SELECT.ParamByName('id_cfdi').AsString := cfdi.ToString;
        CONN.SQL_SELECT.ParamByName('colonia').AsString := colonia;
        CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
        CONN.SQL_SELECT.ParamByName('direccion').AsString := direccion;
        CONN.SQL_SELECT.ParamByName('id_pais').AsString := id_pais.ToString;
        CONN.SQL_SELECT.ParamByName('id_estado').AsString := id_estado.ToString;
        CONN.SQL_SELECT.ParamByName('id_municipio').AsString := id_municipio.ToString;

        CONN.SQL_SELECT.ExecSQL;

        if (CONN.SQL_SELECT.RowsAffected > 0) then
          begin
            ShowMessage('�Se ha registrado la informaci�n!');
            frame_Cliente1.Visible:= False;
            cargartabla;
          end
        else
          begin
            ShowMessage('Algo ha salido mal...');
          end;

  end;

end;

end.
