unit RegistroPais;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, Datamodule,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation;

type
  TFrame_RegPais = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    edt_Nombre: TEdit;
    lbs_Nombre: TLabel;
    edt_iso: TEdit;
    lbs_so: TLabel;
    procedure cargardatos;
  private
    { Private declarations }
  public
    { Public declarations }
    var
      id: integer;
  end;

implementation

{$R *.fmx}

  procedure TFrame_RegPais.cargardatos;
  begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, a.iso  FROM paises a where a.id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;



    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      edt_Nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      edt_iso.Text := CONN.SQL_SELECT.FieldByName('iso').AsString;

    end;
  end;

end.
