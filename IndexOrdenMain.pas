unit IndexOrdenMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, OrdenDeCompra, FMX.Grid, FMX.ScrollBox,
  FMX.Objects, FMX.Controls.Presentation, datamodule;

type
  Tfrm_IndexOrden = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_indexOrden: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn6: TStringColumn;
    OrdenCompra1: TOrdenCompra;
    procedure btn_agregarClick(Sender: TObject);
    procedure cargartabla;
    procedure OrdenCompra1btn_cancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure Tfrm_IndexOrden.btn_agregarClick(Sender: TObject);
begin
OrdenCompra1.Visible:=true;
rec_container.Visible:=false;
 CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT INTO `transacciones`(`id`, `tipo`, `fecha`, `folio`) VALUES (null, :tipo, CURRENT_DATE, :folio)';

            CONN.SQL_SELECT.ParamByName('tipo').AsString := 'c';
            CONN.SQL_SELECT.ParamByName('folio').AsString := 'd11';

            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then
              begin
                ShowMessage('�Se han actualizado los datos!');
                OrdenCompra1.cargardatos;

              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;

end;

procedure Tfrm_IndexOrden.cargartabla;
  var
    I: integer;
  begin
     //Select en grid



    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }           //Select en grid
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.folio, b.nombre, a.cantidad_total, a.descuento, a.notas, a.estado, a.fecha FROM transacciones a JOIN proveedores b ON a.id_proveedor = b.id JOIN empleados c on a.id_empleado=c.id WHERE c.id=1';

      CONN.SQL_SELECT.Open;

      if CONN.SQL_SELECT.RecordCount >0 then
      begin


        for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do

          begin
            Grd_IndexOrden.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
            Grd_IndexOrden.Cells[0, I] := CONN.SQL_SELECT.FieldByName('folio').AsString;
            Grd_IndexOrden.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
            Grd_IndexOrden.Cells[2, I] := CONN.SQL_SELECT.FieldByName('cantidad_total').AsString;
            Grd_IndexOrden.Cells[3, I] := CONN.SQL_SELECT.FieldByName('notas').AsString;
            Grd_IndexOrden.Cells[4, I] := CONN.SQL_SELECT.FieldByName('estado').AsString;
            Grd_IndexOrden.Cells[5, I] := CONN.SQL_SELECT.FieldByName('fecha').AsString;

            CONN.SQL_SELECT.Next;
          end;
      end;
    end;

procedure Tfrm_IndexOrden.OrdenCompra1btn_cancelarClick(Sender: TObject);
begin
  OrdenCompra1.btn_cancelarClick(Sender);
    OrdenCompra1.Visible:=false;
  rec_container.Visible:=true;
  ordenCompra1.limpiardatos;

end;

end.
