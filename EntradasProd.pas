unit EntradasProd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.ListBox,
  FMX.DateTimeCtrls, FMX.Edit, FMX.Objects, FMX.Controls.Presentation;

type
  TFrame6 = class(TFrame)
    rec_container: TRectangle;
    lbl_entrada: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    lbl_alm: TLabel;
    lbl_fecha: TLabel;
    lbl_client: TLabel;
    combo_client: TComboBox;
    lbl_obser: TLabel;
    edt_obser: TEdit;
    rec_botones: TRectangle;
    Grd_entradas: TGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    btn_Aceptar: TCornerButton;
    brn_Cancelar: TCornerButton;
    DateEdit1: TDateEdit;
    lbl_productos: TText;
    edt_cantidad: TEdit;
    combo_productos: TComboBox;
    lbl_cantidad: TText;
    combo_alm: TComboBox;
    lbl_Detalles: TText;
    btn_Agregar: TCornerButton;
    btn_Borrar: TCornerButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

end.
