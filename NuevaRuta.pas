unit NuevaRuta;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.DateTimeCtrls, FMX.Objects,
  FMX.ListBox, Datamodule;

type
  Tframe_NuevaRuta = class(TFrame)
    rect_container: TRectangle;
    lbl_nuevaruta: TText;
    Line1: TLine;
    Rectangle1: TRectangle;
    lbl_fecharuta: TText;
    DateEdit1: TDateEdit;
    lbl_cliente: TText;
    lbl_direccion: TText;
    edt_direccion: TEdit;
    lbl_cp: TText;
    lbl_colonia: TText;
    edt_colonia: TEdit;
    edt_cp: TEdit;
    lbl_pais: TText;
    cb_pais: TComboBox;
    lbl_estado: TText;
    cb_estado: TComboBox;
    lbl_municipio: TText;
    cb_municipio: TComboBox;
    lbl_empleado: TText;
    edt_empleado: TEdit;
    lbl_pedido: TText;
    edt_pedido: TEdit;
    btn_aceptar: TCornerButton;
    btn_cancelar: TCornerButton;
    cb_cliente: TComboBox;

    //function cargardatos(nombre: string): string;
    procedure limpiardatos;
    procedure btn_cancelarClick(Sender: TObject);
    procedure btn_aceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure Tframe_NuevaRuta.btn_aceptarClick(Sender: TObject);
  var
    Empleado, Direccion, Colonia, Pedido, Municipio, Estado, CP, Pais: String;
  begin
    Empleado := edt_empleado.Text;
    Direccion := edt_direccion.Text;
    Colonia := edt_colonia.Text;
    Pedido := edt_pedido.Text;
    CP := edt_cP.Text;

    if (Empleado = '') or (Direccion = '') or (Colonia = '') or (Pedido = '') or (Municipio = '') or (Estado = '') or (CP = '') then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
      CONN.SQL_SELECT.Close;
      CONN.SQL_SELECT.SQL.Text := 'INSERT `ruta_detalle` SET `nombre`=:Nombre,`direccion`=:Direccion,`colonia`=:Colonia';
      CONN.SQL_SELECT.ParamByName('Direccion').AsString := Direccion;
      CONN.SQL_SELECT.ParamByName('Colonia').AsString := Colonia;
      CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
      CONN.SQL_SELECT.ExecSQL;

      if CONN.SQL_SELECT.RowsAffected>0 then
      begin
        ShowMessage('�Se han actualizado los datos!');
      end
      else
      begin
        ShowMessage('�Error en la consulta!');
      end;
    end;
end;

procedure Tframe_NuevaRuta.btn_cancelarClick(Sender: TObject);
begin
  self.Visible := False;
end;

procedure Tframe_NuevaRuta.limpiardatos;
begin
  edt_direccion.text := '';
  edt_cp.text := '';
  edt_empleado.text := '';
  cb_pais.Items.Clear;
  cb_estado.Items.Clear;
  cb_municipio.Items.Clear;
end;

end.
