unit IndexUsuarios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, DataModule, FMX.Edit, michiUtils;

type
  Tfrm_index_usuarios = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    grd_usuarios: TStringGrid;
    StringColumn2: TStringColumn;
    StringColumn1: TStringColumn;
    btn_agregar: TCornerButton;
    pl_new: TPanel;
    lbl_usuario: TLabel;
    edt_usuario: TEdit;
    lbl_password: TLabel;
    edt_password: TEdit;
    lbl_correo: TLabel;
    edt_correo: TEdit;
    cornbtn_formu2: TCornerButton;
    cornbtn_formu1: TCornerButton;
    Image2: TImage;
    Image4: TImage;
    Image3: TImage;
    procedure cargartabla;
    procedure btn_agregarClick(Sender: TObject);
    procedure cornbtn_formu1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure Tfrm_index_usuarios.btn_agregarClick(Sender: TObject);
begin
 pl_new.Visible := True;
end;

procedure Tfrm_index_usuarios.cargartabla;
var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.usuario, a.correo FROM usuarios a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin

          grd_usuarios.Cells[0, I] := CONN.SQL_SELECT.FieldByName('usuario').AsString;
          grd_usuarios.Cells[1, I] := CONN.SQL_SELECT.FieldByName('correo').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end;
end;
procedure Tfrm_index_usuarios.cornbtn_formu1Click(Sender: TObject);
var
Usuario, Password, Correo: String;
correctEmail: Boolean;
begin
  Usuario:= edt_usuario.Text;
  Password:= edt_password.Text;
  Correo:= edt_correo.Text;

  if (Usuario = '') or (Password = '') or (Correo = '') then
    begin
      ShowMessage('�No puedes dejar informaci�n vacia!');
    end
  else
    begin
     //Prepare password
     Password:= md5(Password);
      correctEmail := michiUtils.IsValidEmailRegEx(Correo);
      if correctEmail then
      begin
        CONN.SQL_SELECT.Close;
            CONN.SQL_SELECT.SQL.Text := 'INSERT `usuarios` SET `usuario`=:Usuario,`pass`=:Password,`correo`=:Correo';

            CONN.SQL_SELECT.ParamByName('Usuario').AsString := Usuario;
            CONN.SQL_SELECT.ParamByName('Password').AsString := Password;
            CONN.SQL_SELECT.ParamByName('Correo').AsString := Correo;
            CONN.SQL_SELECT.ExecSQL;

            if CONN.SQL_SELECT.RowsAffected>0 then

              begin
                ShowMessage('�Se han actualizado los datos!');
                edt_usuario.Text:='';
                edt_password.Text:='';
                edt_correo.Text:='';
                self.cargartabla;
                pl_new.Visible:=False;

              end
            else
              begin
                ShowMessage('�Error en la consulta!');
              end;
      end
      else
        ShowMessage('Correo inv�lido');

    end;
end;

end.
