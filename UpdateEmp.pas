unit UpdateEmp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Objects, FMX.Controls.Presentation, DataModule;

type
  TFrame_UpdateEmp = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    cb_Estados: TComboBox;
    cb_Municipio: TComboBox;
    edt_Colonia: TEdit;
    edt_CP: TEdit;
    edt_Direccion: TEdit;
    edt_Nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Estado: TLabel;
    lbs_Municipio: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    lbs_regimen: TLabel;
    edt_rfc: TEdit;
    lbs_RFC: TLabel;
    edt_razonsocial: TEdit;
    lbs_razon: TLabel;
    cb_regimen: TComboBox;
    procedure cargardatos;
  private
    { Private declarations }
  public
    { Public declarations }
    var
    id: integer;
  end;

implementation

{$R *.fmx}

  procedure TFrame_UpdateEmp.cargardatos;
  begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre, b.c_RegimenFiscal AS ''cod'', b.`Descripción` AS ''desc'', a.razon_social, a.rfc, a.direccion, a.colonia, a.CP, a.telefono FROM empresa a JOIN f4_c_regimenfiscal b ON a.id_regimen = b.id WHERE a.id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin
      id := CONN.SQL_SELECT.FieldByName('id').AsInteger;
      edt_Nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      edt_razonsocial.Text := CONN.SQL_SELECT.FieldByName('razon_social').AsString;
      edt_rfc.Text :=  CONN.SQL_SELECT.FieldByName('rfc').AsString;
      edt_Direccion.Text := CONN.SQL_SELECT.FieldByName('direccion').AsString;
      edt_Colonia.Text :=  CONN.SQL_SELECT.FieldByName('colonia').AsString;
      edt_cp.Text := CONN.SQL_SELECT.FieldByName('cp').AsString;

    end;
  end;

end.
