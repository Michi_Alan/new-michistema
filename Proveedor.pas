﻿unit Proveedor;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Objects, FMX.Edit, FMX.ListBox, FMX.Layouts,
   FireDAC.UI.Intf, FireDAC.FMXUI.Wait, FireDAC.Stan.Intf,
  FireDAC.Comp.UI, Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope;

type
  TFrame_proveedor = class(TFrame)
    Fondo: TRectangle;
    Label2: TLabel;
    Edit_nombre: TEdit;
    Edit_direccion: TEdit;
    Edit_rfc: TEdit;
    Edit_cp: TEdit;
    Edit_correo: TEdit;
    Edit_colonia: TEdit;
    Lab_Formu: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Select_municipio: TComboBox;
    contenido: TRectangle;
    contenido1: TRectangle;
    Select_estado: TComboBox;
    btn_guardar: TCornerButton;
    btn_cancelar: TCornerButton;
    Line1: TLine;
    Rectangle1: TRectangle;
    Edit_agente: TEdit;
    Edit_telefono: TEdit;
    Select_pais: TComboBox;
    btn_actualizar: TCornerButton;
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);
    procedure Select_estadoClosePopup(Sender: TObject);
    procedure btn_actualizarClick(Sender: TObject);


  private

    { Private declarations }
  public
    { Public declarations }
    procedure  limpiar();

    procedure cargar_paises;

    procedure cargardatos_estados;
    procedure cargardatos_municipio;

    procedure cargardatos_proveedores;
    var
     id: integer;

  end;

implementation

{$R *.fmx}

uses DataModule;



procedure TFrame_proveedor.cargardatos_estados;

begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM estados WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin

    Select_estado.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;



end;
procedure TFrame_proveedor.cargardatos_municipio;
begin
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin

    Select_municipio.Items.Insert(CONN.SQL_SELECT.FieldByName('id').AsInteger, CONN.SQL_SELECT.FieldByName('nombre').AsString);
    CONN.SQL_SELECT.Next;
  end;


end;


procedure TFrame_proveedor.cargardatos_proveedores;
var x:integer;
begin
 begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT * FROM proveedores  where id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;


    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      Edit_nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      Edit_rfc.Text := CONN.SQL_SELECT.FieldByName('rfc').AsString;
      Edit_telefono.Text := CONN.SQL_SELECT.FieldByName('telefono').AsString;
      Edit_direccion.Text := CONN.SQL_SELECT.FieldByName('direccion').AsString;
      Edit_colonia.Text := CONN.SQL_SELECT.FieldByName('colonia').AsString;
      Edit_correo.Text := CONN.SQL_SELECT.FieldByName('correo').AsString;
      Edit_cp.Text := CONN.SQL_SELECT.FieldByName('cp').AsString;
      Edit_agente.Text := CONN.SQL_SELECT.FieldByName('nombre_contacto').AsString;

      for x := 0 to Select_estado.Items.Count-1 do
      begin
        if Select_estado.Items[x]= CONN.SQL_SELECT.FieldByName('id_estado').AsString then
        begin
          Select_estado.ItemIndex:=x;
        end;

      end;


      for x := 0 to Select_municipio.Items.Count-1 do
      begin
        if Select_municipio.Items[x]= CONN.SQL_SELECT.FieldByName('id_municipio').AsString then
        begin
          Select_municipio.ItemIndex:=x;
        end;

      end;

      for x := 0 to Select_pais.Items.Count-1 do
      begin
        if Select_pais.Items[x]= CONN.SQL_SELECT.FieldByName('id_pais').AsString then
        begin
          Select_pais.ItemIndex:=x;
        end;

      end;


    end;
  end;
end;

procedure TFrame_proveedor.cargar_paises;
begin

  CONN.SQL_SELECT.SQL.Clear;
  CONN.SQL_SELECT.SQL.Add('SELECT id, nombre FROM paises');
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_pais.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;

end;


procedure TFrame_proveedor.btn_actualizarClick(Sender: TObject);

var
 id_municipio,id_estado,id_pais: integer;
 nombre, rfc, tel, direc, col,correo, CP, nombreComtacto: String;
begin


//Validaciones de text nombre
////El Trim quita los espacios que hay ese campo
    Edit_nombre.Text := Trim(Edit_nombre.Text);
    Edit_agente.Text := Trim(Edit_agente.Text);
    Edit_direccion.Text := Trim(Edit_direccion.Text);
    Edit_telefono.Text := Trim(Edit_telefono.Text);
    Edit_colonia.Text := Trim(Edit_colonia.Text);
    Edit_rfc.Text := Trim(Edit_rfc.Text);
    Edit_correo.Text := Trim(Edit_correo.Text);
    Edit_cp.Text := Trim(Edit_cp.Text);

if Length(Edit_nombre.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el nombre !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_nombre.SetFocus;
   Exit;
end;
 if Length(Edit_agente.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el nombre de contacto !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_agente.SetFocus;
   Exit;
end;
if Length(Edit_direccion.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la direcci�n !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_direccion.SetFocus;
   Exit;
end;
if Length(Edit_telefono.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el tel�fono !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_telefono.SetFocus;
   Exit;
end;
if Length(Edit_colonia.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la colonia !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_colonia.SetFocus;
   Exit;
end;
if Length(Edit_rfc.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el RFC !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_rfc.SetFocus;
   Exit;
end;
if Length(Edit_correo.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el correo !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_correo.SetFocus;
   Exit;
end;
if Length(Edit_cp.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el C�digo postal !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_cp.SetFocus;
   Exit;
end;
  //se cierra la validacion de los inputs
  //valicacion de select
if Select_pais.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar un pais !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_pais.SetFocus;
   Exit;
end;
if Select_estado.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar el estado !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_estado.SetFocus;
   Exit;
end;

if Select_municipio.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar un municipio !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_municipio.SetFocus;
   Exit;
end;


if btn_actualizar.Enabled = true then
  begin

  //campos que necesita ara agregar
   id_estado := Select_estado.Selected.Index+1;
   id_municipio := Select_municipio.Selected.Index+1;
   id_pais := Select_pais.Selected.Index+1;

    nombre := Edit_Nombre.Text;
    rfc :=  Edit_rfc.Text;
    tel :=  Edit_telefono.Text;
    direc :=   Edit_direccion.Text;
    col :=     Edit_colonia.Text;
    correo :=   Edit_correo.Text;
    CP :=   Edit_cp.Text;
    nombreComtacto := Edit_agente.Text;

    if btn_actualizar.Enabled=True then
    begin
         //guardar proveedor

    CONN.SQL_SELECT.Close;

    CONN.SQL_SELECT.SQL.Text := 'UPDATE proveedores  set nombre = :nombre, rfc= :rfc, telefono=:telefono,' +
    'direccion =:direccion,colonia= :colonia, correo=:correo, cp=:CP, id_municipio=:id_municipio, ' +
    'id_estado = :estado, id_pais = :pais,nombre_contacto= :nombre_contacto WHERE id= :id';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('rfc').AsString := rfc;
    CONN.SQL_SELECT.ParamByName('telefono').AsString := tel;
    CONN.SQL_SELECT.ParamByName('direccion').AsString := direc;
    CONN.SQL_SELECT.ParamByName('colonia').AsString := col;
    CONN.SQL_SELECT.ParamByName('correo').AsString := correo;
    CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
    CONN.SQL_SELECT.ParamByName('id_municipio').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
    CONN.SQL_SELECT.ParamByName('nombre_contacto').AsString := nombreComtacto;
     CONN.SQL_SELECT.ParamByName('id').AsInteger := id;


    CONN.SQL_SELECT.ExecSQL;
    if (CONN.SQL_SELECT.RowsAffected > 0) then

    begin
      ShowMessage('�Se ha registrado el proveedor!:  ' +Edit_nombre.Text);
      limpiar();

    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;

    end
    else
    begin
      //aqui para editar
    end;

    end
else
begin
  //aqui actualizar

end;
end;


procedure TFrame_proveedor.btn_cancelarClick(Sender: TObject);
begin
limpiar;
end;

procedure TFrame_proveedor.btn_guardarClick(Sender: TObject);

var
 id_municipio,id_estado,id_pais: integer;
 nombre, rfc, tel, direc, col,correo, CP, nombreComtacto: String;
begin


//Validaciones de text nombre
////El Trim quita los espacios que hay ese campo
    Edit_nombre.Text := Trim(Edit_nombre.Text);
    Edit_agente.Text := Trim(Edit_agente.Text);
    Edit_direccion.Text := Trim(Edit_direccion.Text);
    Edit_telefono.Text := Trim(Edit_telefono.Text);
    Edit_colonia.Text := Trim(Edit_colonia.Text);
    Edit_rfc.Text := Trim(Edit_rfc.Text);
    Edit_correo.Text := Trim(Edit_correo.Text);
    Edit_cp.Text := Trim(Edit_cp.Text);

if Length(Edit_nombre.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el nombre !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_nombre.SetFocus;
   Exit;
end;
 if Length(Edit_agente.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el nombre de contacto !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_agente.SetFocus;
   Exit;
end;
if Length(Edit_direccion.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar la dirección !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_direccion.SetFocus;
   Exit;
end;
if Length(Edit_telefono.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el teléfono !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_telefono.SetFocus;
   Exit;
end;
if Length(Edit_colonia.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar la colonia !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_colonia.SetFocus;
   Exit;
end;
if Length(Edit_rfc.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el RFC !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_rfc.SetFocus;
   Exit;
end;
if Length(Edit_correo.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el correo !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_correo.SetFocus;
   Exit;
end;
if Length(Edit_cp.Text)= 0 then
begin
    MessageDlg('¡Tienes que agregar el Código postal !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_cp.SetFocus;
   Exit;
end;
  //se cierra la validacion de los inputs
  //valicacion de select
if Select_pais.ItemIndex = -1 then
begin
  MessageDlg('¡Tienes que seleccionar un pais !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_pais.SetFocus;
   Exit;
end;
if Select_estado.ItemIndex = -1 then
begin
  MessageDlg('¡Tienes que seleccionar el estado !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_estado.SetFocus;
   Exit;
end;

if Select_municipio.ItemIndex = -1 then
begin
  MessageDlg('¡Tienes que seleccionar un municipio !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Select_municipio.SetFocus;
   Exit;
end;


if btn_guardar.Enabled = true then
  begin

  //campos que necesita ara agregar
   id_estado := Select_estado.Selected.Index+1;
   id_municipio := Select_municipio.Selected.Index+1;
   id_pais := Select_pais.Selected.Index+1;

    nombre := Edit_Nombre.Text;
    rfc :=  Edit_rfc.Text;
    tel :=  Edit_telefono.Text;
    direc :=   Edit_direccion.Text;
    col :=     Edit_colonia.Text;
    correo :=   Edit_correo.Text;
    CP :=   Edit_cp.Text;
    nombreComtacto := Edit_agente.Text;

    if btn_guardar.Enabled=True then
    begin
         //guardar proveedor

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO proveedores (nombre, rfc, telefono, direccion, colonia, correo, cp, id_municipio, id_estado, id_pais,nombre_contacto) VALUES (:nombre, :rfc, :telefono, :direccion, :colonia, :correo, :CP ,:id_municipio, :estado, :pais, :nombre_contacto)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('rfc').AsString := rfc;
    CONN.SQL_SELECT.ParamByName('telefono').AsString := tel;
    CONN.SQL_SELECT.ParamByName('direccion').AsString := direc;
    CONN.SQL_SELECT.ParamByName('colonia').AsString := col;
    CONN.SQL_SELECT.ParamByName('correo').AsString := correo;
    CONN.SQL_SELECT.ParamByName('CP').AsString := CP;
    CONN.SQL_SELECT.ParamByName('id_municipio').AsString := id_municipio.ToString;
    CONN.SQL_SELECT.ParamByName('estado').AsString := id_estado.ToString;
    CONN.SQL_SELECT.ParamByName('pais').AsString := id_pais.ToString;
    CONN.SQL_SELECT.ParamByName('nombre_contacto').AsString := nombreComtacto;


    CONN.SQL_SELECT.ExecSQL;
    if (CONN.SQL_SELECT.RowsAffected > 0) then

    begin
      ShowMessage('¡Se ha registrado el proveedor!:  ' +Edit_nombre.Text);
      limpiar();

    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;

    end
    else
    begin
      //aqui para editar
    end;

    end
else
begin
  //aqui actualizar
end;


////
////
////
////
////      if (Edit_nombre.Text = '') or (Edit_agente.Text = '') or (Edit_direccion.Text = '')
////      or (Edit_telefono.Text = '') or (Edit_colonia.Text = '') or (Edit_rfc.Text = '')
////      or (Edit_correo.Text = '') or (Edit_cp.Text = '') or (Select_pais.ItemIndex = -1)
////      or (Select_estado.ItemIndex = -1) or (Select_municipio.ItemIndex = -1) then
//    begin
//
//      MessageDlg('¡No puedes dejar información vacia!',TMsgDlgType.mtWarning, mbYesNo, 0);
//
//    end
//    else


end;

procedure TFrame_proveedor.limpiar;
begin
//Lipiar lo campos
Edit_nombre.Text := '';
Edit_agente.Text := '';
Edit_direccion.Text := '';
Edit_telefono.Text := '';
Edit_colonia.Text := '';
Edit_rfc.Text := '';
Edit_correo.Text := '';
Edit_cp.Text := '';

Select_estado.clear;
Select_municipio.clear;
Select_pais.clear;

end;

procedure TFrame_proveedor.Select_estadoClosePopup(Sender: TObject);
begin
  Select_municipio.Items.Clear;
  Select_municipio.Enabled := True;


  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

  CONN.SQL_SELECT.ParamByName('id').AsInteger := Select_estado.Selected.Index+1;
  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    Select_municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    Select_municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
end;

end.





