unit Articulos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox, Data.DB;

type
  TFrame_productos_articulos = class(TFrame)
    registra_articulos: TRectangle;
    from: TRectangle;
    Titulo_registra: TLabel;
    Line1: TLine;
    Rectangle1: TRectangle;
    Edit_nombre: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    btn_cancelar: TCornerButton;
    btn_guardar: TCornerButton;
    Select_unidad: TComboBox;
    Label10: TLabel;
    Edit_preciomayoreo: TEdit;
    Edit_cantidad: TEdit;
    Edit_marca: TEdit;
    Edit_precio: TEdit;
    Edit_descripcion: TEdit;
    select_proveedor: TComboBox;
    select_tipo: TComboBox;
    select_categoria: TComboBox;
    Label11: TLabel;
    btn_actualizar: TCornerButton;
    che_AI: TCheckBox;
    Edit_descuento: TEdit;
    Label3: TLabel;
    procedure btn_guardarClick(Sender: TObject);
    procedure btn_actualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure cargar_proveedores;
    //para limpiar el campo
    procedure  limpiar_campo();
    procedure cargar_categoria_productos;
    procedure cargar_tipo;
    procedure cargar_unidad;
     //mostrar los datos por defaul por id
    procedure cargardatos_productos;
       var
      id: integer;
  end;

implementation

{$R *.fmx}

uses DataModule;

procedure TFrame_productos_articulos.btn_actualizarClick(Sender: TObject);
var
 id_proveedor,id_categoria, tipo, unidad_medida: integer;
 nombre, precio_unico, precio_mayoreo, marca, descuento, cantidad, descripcion: String;
begin
//Validaciones de text nombre
////El Trim quita los espacios que hay ese campo

  Edit_nombre.Text := Trim(Edit_nombre.Text);
  Edit_preciomayoreo.Text := Trim(Edit_preciomayoreo.Text);
  Edit_cantidad.Text := Trim(Edit_cantidad.Text);
  Edit_marca.Text := Trim(Edit_marca.Text);;
  Edit_precio.Text := Trim(Edit_precio.Text);
  Edit_descripcion.Text := Trim(Edit_descripcion.Text);
  Edit_descuento.Text := Trim(Edit_descuento.Text);

if Length(Edit_nombre.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el nombre !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_nombre.SetFocus;
   Exit;
end;
if Length(Edit_descuento.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el descuento !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_descuento.SetFocus;
   Exit;
end;
 if Length(Edit_preciomayoreo.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el precio de mayoreo!',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_preciomayoreo.SetFocus;
   Exit;
end;
if Length(Edit_cantidad.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la cantidad !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_cantidad.SetFocus;
   Exit;
end;


if Length(Edit_marca.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la marca !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_marca.SetFocus;
   Exit;
end;
if Length(Edit_precio.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el precio !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_precio.SetFocus;
   Exit;
end;
if Length(Edit_descripcion.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar una descripcion !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_descripcion.SetFocus;
   Exit;
end;

  //se cierra la validacion de los inputs
  //valicacion de select
if select_proveedor.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar un proveedor !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_proveedor.SetFocus;
   Exit;
end;
if select_tipo.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar el tipo !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_tipo.SetFocus;
   Exit;
end;

if select_categoria.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar una categoria !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_categoria.SetFocus;
   Exit;
end;

if select_unidad.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar la unidad de medida !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_unidad.SetFocus;
   Exit;
end;



if btn_actualizar.Enabled = true then
  begin

  //campos que necesita ara agregar
   id_proveedor := select_proveedor.Selected.Index+1;
   id_categoria := select_categoria.Selected.Index+1;
   tipo := select_tipo.Selected.Index+1;
   unidad_medida := select_unidad.Selected.Index+1;

    nombre := Edit_Nombre.Text;
    precio_unico :=  Edit_precio.Text;
    precio_mayoreo :=  Edit_preciomayoreo.Text;
    marca :=   Edit_marca.Text;
    cantidad :=     Edit_cantidad.Text;
    descripcion :=   Edit_descripcion.Text;
    descuento :=   Edit_descuento.Text;


    if btn_actualizar.Enabled=True then
    begin
         //guardar proveedor

    CONN.SQL_SELECT.Close;

    CONN.SQL_SELECT.SQL.Text := 'UPDATE productos set id_proveedor = :id_proveedor, id_categoria = :id_categoria, ' +
    'nombre= :nombre, unidad_medida= :unidad_medida, tipo= :tipo, cantidad= :cantidad, ' +
    'marca= :marca, precio_mayoreo= :precio_mayoreo, precio_unico = :precio_unico, ' +
    'descripcion= :descripcionc, descuento :descuento WHERE id= :id';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('cantidad').AsString := cantidad;
    CONN.SQL_SELECT.ParamByName('marca').AsString := marca;
    CONN.SQL_SELECT.ParamByName('precio_mayoreo').AsString := precio_mayoreo;
    CONN.SQL_SELECT.ParamByName('precio_unico').AsString := precio_unico;
    CONN.SQL_SELECT.ParamByName('descripcion').AsString := descripcion;
    CONN.SQL_SELECT.ParamByName('id_proveedor').AsString := id_proveedor.ToString;
    CONN.SQL_SELECT.ParamByName('id_categoria').AsString := id_categoria.ToString;
    CONN.SQL_SELECT.ParamByName('unidad_medida').AsString := unidad_medida.ToString;
    CONN.SQL_SELECT.ParamByName('tipo').AsString := tipo.ToString;
    CONN.SQL_SELECT.ParamByName('descuento').AsString := descuento;
    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;



    CONN.SQL_SELECT.ExecSQL;
    if (CONN.SQL_SELECT.RowsAffected > 0) then

    begin
      ShowMessage('�Se ha Actualizado el producto!:  ' +Edit_nombre.Text);
      limpiar_campo();

    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;

    end;
  end;


end;


procedure TFrame_productos_articulos.btn_guardarClick(Sender: TObject);
var
 id_proveedor,id_categoria, tipo, unidad_medida: integer;
 nombre, precio_unico, precio_mayoreo, descuento,marca, cantidad, descripcion: String;
begin
//Validaciones de text nombre
////El Trim quita los espacios que hay ese campo

  Edit_nombre.Text := Trim(Edit_nombre.Text);
  Edit_preciomayoreo.Text := Trim(Edit_preciomayoreo.Text);
  Edit_cantidad.Text := Trim(Edit_cantidad.Text);
  Edit_marca.Text := Trim(Edit_marca.Text);;
  Edit_precio.Text := Trim(Edit_precio.Text);
  Edit_descripcion.Text := Trim(Edit_descripcion.Text);
  Edit_descuento.Text :=  Trim(Edit_descuento.Text);


if Length(Edit_nombre.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el nombre !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_nombre.SetFocus;
   Exit;
end;
if Length(Edit_descuento.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el descuento !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_descuento.SetFocus;
   Exit;
end;
 if Length(Edit_preciomayoreo.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el precio de mayoreo!',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_preciomayoreo.SetFocus;
   Exit;
end;
if Length(Edit_cantidad.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la cantidad !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_cantidad.SetFocus;
   Exit;
end;


if Length(Edit_marca.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar la marca !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_marca.SetFocus;
   Exit;
end;
if Length(Edit_precio.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar el precio !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_precio.SetFocus;
   Exit;
end;
if Length(Edit_descripcion.Text)= 0 then
begin
    MessageDlg('�Tienes que agregar una descripcion !',TMsgDlgType.mtWarning, mbYesNo, 0);
   Edit_descripcion.SetFocus;
   Exit;
end;

  //se cierra la validacion de los inputs
  //valicacion de select
if select_proveedor.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar un proveedor !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_proveedor.SetFocus;
   Exit;
end;
if select_tipo.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar el tipo !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_tipo.SetFocus;
   Exit;
end;

if select_categoria.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar una categoria !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_categoria.SetFocus;
   Exit;
end;

if select_unidad.ItemIndex = -1 then
begin
  MessageDlg('�Tienes que seleccionar la unidad de medida !',TMsgDlgType.mtWarning, mbYesNo, 0);
   select_unidad.SetFocus;
   Exit;
end;



if btn_guardar.Enabled = true then
  begin

  //campos que necesita ara agregar
   id_proveedor := Select_proveedor.Selected.Index+1;
   id_categoria := Select_categoria.Selected.Index+1;
   tipo := select_tipo.Selected.Index+1;
   unidad_medida := select_unidad.Selected.Index+1;

    nombre := Edit_Nombre.Text;
    precio_unico :=  Edit_precio.Text;
    precio_mayoreo :=  Edit_preciomayoreo.Text;
    marca :=   Edit_marca.Text;
    cantidad :=     Edit_cantidad.Text;
    descripcion :=   Edit_descripcion.Text;
    descuento :=   Edit_descuento.Text;


    if btn_guardar.Enabled=True then
    begin
         //guardar proveedor

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'INSERT INTO productos (id_proveedor, id_categoria, ' +
    'nombre, unidad_medida, tipo, cantidad, marca, precio_mayoreo, precio_unico, descuento, descripcion) ' +
    'VALUES (:id_proveedor, :id_categoria, :nombre, :unidad_medida, :tipo, :cantidad, :marca , ' +
    ':precio_mayoreo, :precio_unico, :descuento, :descripcion)';

    CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
    CONN.SQL_SELECT.ParamByName('cantidad').AsString := cantidad;
    CONN.SQL_SELECT.ParamByName('marca').AsString := marca;
    CONN.SQL_SELECT.ParamByName('precio_mayoreo').AsString := precio_mayoreo;
    CONN.SQL_SELECT.ParamByName('precio_unico').AsString := precio_unico;
    CONN.SQL_SELECT.ParamByName('descripcion').AsString := descripcion;
    CONN.SQL_SELECT.ParamByName('id_proveedor').AsString := id_proveedor.ToString;
    CONN.SQL_SELECT.ParamByName('id_categoria').AsString := id_categoria.ToString;
    CONN.SQL_SELECT.ParamByName('unidad_medida').AsString := unidad_medida.ToString;
    CONN.SQL_SELECT.ParamByName('descuento').AsString := descuento;
    CONN.SQL_SELECT.ParamByName('tipo').AsString := tipo.ToString;



    CONN.SQL_SELECT.ExecSQL;
    if (CONN.SQL_SELECT.RowsAffected > 0) then

    begin
      ShowMessage('�Se ha registrado el producto!:  ' +Edit_nombre.Text);
      limpiar_campo();

    end
    else
    begin
      ShowMessage('Algo ha salido mal...');
    end;

    end;
  end;


end;

procedure TFrame_productos_articulos.cargardatos_productos;
var x:integer;
begin
 begin
    CONN.SQL_SELECT.close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT * FROM productos WHERE id = :id;';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := id;
    CONN.SQL_SELECT.Open;


    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      Edit_nombre.Text := CONN.SQL_SELECT.FieldByName('nombre').AsString;
      Edit_preciomayoreo.Text := CONN.SQL_SELECT.FieldByName('precio_mayoreo').AsString;
      Edit_marca.Text := CONN.SQL_SELECT.FieldByName('marca').AsString;
      Edit_cantidad.Text := CONN.SQL_SELECT.FieldByName('cantidad').AsString;
      Edit_precio.Text := CONN.SQL_SELECT.FieldByName('precio_unico').AsString;
      Edit_descripcion.Text := CONN.SQL_SELECT.FieldByName('descripcion').AsString;
       Edit_descuento.Text := CONN.SQL_SELECT.FieldByName('descuento').AsString;


      for x := 0 to select_proveedor.Items.Count-1 do
      begin
        if select_proveedor.Items[x]= CONN.SQL_SELECT.FieldByName('id_proveedor').AsString then
        begin
          select_proveedor.ItemIndex:=x;
        end;

      end;

      for x := 0 to select_tipo.Items.Count-1 do
      begin
        if select_tipo.Items[x]= CONN.SQL_SELECT.FieldByName('tipo').AsString then
        begin
          select_tipo.ItemIndex:=x;
        end;

      end;

      for x := 0 to select_categoria.Items.Count-1 do
      begin
        if select_categoria.Items[x]= CONN.SQL_SELECT.FieldByName('id_categoria').AsString then
        begin
          select_categoria.ItemIndex:=x;
        end;

      end;
        for x := 0 to select_unidad.Items.Count-1 do
      begin
        if select_unidad.Items[x]= CONN.SQL_SELECT.FieldByName('unidad_medida').AsString then
        begin
          select_unidad.ItemIndex:=x;
        end;

      end;


    end;
  end;
end;


procedure TFrame_productos_articulos.cargar_categoria_productos;
begin
    CONN.UniQuery2.Close;
  CONN.UniQuery2.SQL.Text := 'SELECT id, nombre FROM categoria_productos';

  CONN.UniQuery2.Open;

  while not CONN.UniQuery2.Eof do
  begin
    select_categoria.Items.Add(CONN.UniQuery2.FieldByName('nombre').AsString);
    select_categoria.ItemIndex := StrToInt(CONN.UniQuery2.FieldByName('id').AsString);
   CONN.UniQuery2.Next;
  end;
end;

procedure TFrame_productos_articulos.cargar_proveedores;
begin
   CONN.UniQuery2.Close;
  CONN.UniQuery2.SQL.Text := 'SELECT id, nombre FROM proveedores';

  CONN.UniQuery2.Open;

  while not CONN.UniQuery2.Eof do
  begin
    select_proveedor.Items.Add(CONN.UniQuery2.FieldByName('nombre').AsString);
    select_proveedor.ItemIndex := StrToInt(CONN.UniQuery2.FieldByName('id').AsString);
   CONN.UniQuery2.Next;
  end;
end;

procedure TFrame_productos_articulos.cargar_tipo;
begin
    CONN.UniQuery2.Close;
  CONN.UniQuery2.SQL.Text := 'SELECT id, nombre FROM tipo_productos';

  CONN.UniQuery2.Open;

  while not CONN.UniQuery2.Eof do
  begin
    select_tipo.Items.Add(CONN.UniQuery2.FieldByName('nombre').AsString);
    select_tipo.ItemIndex := StrToInt(CONN.UniQuery2.FieldByName('id').AsString);
   CONN.UniQuery2.Next;
  end;
end;

procedure TFrame_productos_articulos.cargar_unidad;
begin
  CONN.UniQuery2.Close;
  CONN.UniQuery2.SQL.Text := 'SELECT id, nombre FROM unidades_medidas';

  CONN.UniQuery2.Open;

  while not CONN.UniQuery2.Eof do
  begin
    select_unidad.Items.Add(CONN.UniQuery2.FieldByName('nombre').AsString);
    select_unidad.ItemIndex := StrToInt(CONN.UniQuery2.FieldByName('id').AsString);
   CONN.UniQuery2.Next;
  end;
end;

procedure TFrame_productos_articulos.limpiar_campo;
begin
Edit_nombre.Text := '';
Edit_preciomayoreo.Text := '';
Edit_cantidad.Text := '';
Edit_marca.Text := '';
Edit_precio.Text := '';
Edit_descripcion.Text := '';

Select_proveedor.Clear;
Select_tipo.Clear;
Select_categoria.Clear;
Select_unidad.Clear;

end;

end.
