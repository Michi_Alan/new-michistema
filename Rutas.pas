unit Rutas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox,
  FMX.Controls.Presentation, FMX.Edit, FMX.DateTimeCtrls, FMX.Objects, NuevaRuta,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope;

type
  TFrame4 = class(TFrame)
    rect_container: TRectangle;
    lbl_rutas: TText;
    Line1: TLine;
    pan_contenido: TRectangle;
    lbl_fecha: TText;
    DateEdit1: TDateEdit;
    btn_Buscar: TCornerButton;
    lbl_nuevo: TCornerButton;
    frame_NuevaRuta: Tframe_NuevaRuta;
    Grid_rutas: TGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;

    procedure btn_BuscarClick(Sender: TObject);
    procedure cargartabla;
    procedure lbl_nuevoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
      procedure cargar_rutasdetalles;
  end;

implementation

{$R *.fmx}

uses Datamodule;

procedure TFrame4.btn_BuscarClick(Sender: TObject);
begin
  Datamodule.CONN.SQL_SELECT.Close;
  Datamodule.CONN.SQL_SELECT.SQL.Text := 'SELECT id, ruta_id, fecha, hora, direccion, colonia, cp ' +
                                         'FROM "ruta_detalles" ' +
                                         'WHERE fecha = :fecha;';
  Conn.SQL_SELECT.ParamByName('fecha').AsDate := DateEdit1.Date;
  //Especificar busqueda por parametro de fecha
  Datamodule.CONN.SQL_SELECT.Open;
end;

procedure TFrame4.cargartabla;
begin
//var
//  I : integer;
//  begin
//
//    {
//      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
//      click al boton de empresas.
//
//      Su primer proceso es realizar un select de la informaci�n que mostrar�
//      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
//      query
//
//      @Objetos y funciones
//       */
//          /* TStringGrid
//          /* Cells[Columnas, Filas]
//       */
//    }
//    CONN.SQL_SELECT.Close;
//    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, b.fecha, c.nombre, a.direccion, a.colonia, ' +
//                                'a.cp, a.hora, d.nombre as pais, e.nombre as estado, f.nombre ' +
//                                'as municipio FROM ruta_detalles a JOIN rutas b ON b.id = ' +
//                                'a.ruta_id JOIN clientes c ON c.id = a.id_cliente ' +
//                                'JOIN paises d ON d.id = a.id_pais JOIN estados e ON e.id = ' +
//                                'a.id_estado JOIN municipios f ON f.id = a.id_municipio';
//
//    CONN.SQL_SELECT.Open;
//    if CONN.SQL_SELECT.RecordCount >0 then
//    begin
//      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
//        begin
//          Grid_Rutas.Cells[0, I] := CONN.SQL_SELECT.FieldByName('fecha').AsString;
//          Grid_Rutas.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
//          Grid_Rutas.Cells[1, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;
//          Grid_Rutas.Cells[2, I] := CONN.SQL_SELECT.FieldByName('direccion').AsString;
//          Grid_Rutas.Cells[3, I] := CONN.SQL_SELECT.FieldByName('colonia').AsString;
//          Grid_Rutas.Cells[4, I] := CONN.SQL_SELECT.FieldByName('cp').AsString;
//          Grid_Rutas.Cells[5, I] := CONN.SQL_SELECT.FieldByName('hora').AsString;
//          Grid_Rutas.Cells[6, I] := CONN.SQL_SELECT.FieldByName('pais').AsString;
//          Grid_Rutas.Cells[7, I] := CONN.SQL_SELECT.FieldByName('estado').AsString;
//          CONN.SQL_SELECT.Next;
//        end;
//    end;
//
//    if (CONN.SQL_SELECT.RowsAffected > 0) then
//    begin
//      ShowMessage('�Se ha registrado la informaci�n!');
//      frame_NuevaRuta.Visible := False;
//      self.cargartabla;
//    end
//    else
//    begin
//      ShowMessage('Algo ha salido mal...');
  end;

procedure TFrame4.cargar_rutasdetalles;
begin


  CONN.SQL_SELECT.SQL.Clear;
  CONN.SQL_SELECT.SQL.Add('SELECT a.id, b.fecha, c.nombre, a.direccion, a.colonia, ' +
                              'a.cp, a.hora, d.nombre as pais, e.nombre as estado, f.nombre ' +
                              'as municipio FROM ruta_detalles a JOIN rutas b ON b.id = ' +
                              'a.ruta_id JOIN clientes c ON c.id = a.id_cliente ' +
                              'JOIN paises d ON d.id = a.id_pais JOIN estados e ON e.id = ' +
                              'a.id_estado JOIN municipios f ON f.id = a.id_municipio');

  CONN.SQL_SELECT.Open;

end;

procedure TFrame4.lbl_nuevoClick(Sender: TObject);
begin
  frame_NuevaRuta.Visible := True;
end;

end.
