unit RegistroUsuarios;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation,System.RegularExpressions, Formularios, WCrypt2;

type
  Tregistro_usuarios = class(TFrame)
    Rectangle1: TRectangle;
    Label1: TLabel;
    Line1: TLine;
    Panel2: TPanel;
    lbl_usuario: TLabel;
    edt_usuario: TEdit;
    lbl_password: TLabel;
    edt_password: TEdit;
    lbl_correo: TLabel;
    edt_correo: TEdit;
    cornbtn_formu2: TCornerButton;
    cornbtn_formu1: TCornerButton;
    Image1: TImage;
    Image2: TImage;
    Image4: TImage;
    Image3: TImage;
    cb_resetpwd: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DataModule;
{$R *.fmx}

function md5(const Input: String): String;
var
  hCryptProvider: HCRYPTPROV;
  hHash: HCRYPTHASH;
  bHash: array[0..$7f] of Byte;
  dwHashBytes: Cardinal;
  pbContent: PByte;
  i: Integer;
begin
  dwHashBytes := 16;
  pbContent := Pointer(PChar(Input));
  Result := '';
  if CryptAcquireContext(@hCryptProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT or CRYPT_MACHINE_KEYSET) then
  begin
    if CryptCreateHash(hCryptProvider, CALG_MD5, 0, 0, @hHash) then
    begin
      if CryptHashData(hHash, pbContent, Length(Input) * sizeof(Char), 0) then
      begin
        if CryptGetHashParam(hHash, HP_HASHVAL, @bHash[0], @dwHashBytes, 0) then
        begin
          for i := 0 to dwHashBytes - 1 do
          begin
            Result := Result + Format('%.2x', [bHash[i]]);
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    CryptReleaseContext(hCryptProvider, 0);
  end;
  Result := AnsiLowerCase(Result);
end;





end.
