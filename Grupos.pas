unit Grupos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, DataModule,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, RegistroGrupos;

type
  TIndexGrupos = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    grd_grupos: TStringGrid;
    StringColumn1: TStringColumn;
    frm_registrar: Tregistro_grupos;
    procedure cargartabla;
    procedure btn_agregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}
procedure TIndexGrupos.btn_agregarClick(Sender: TObject);
begin
  frm_registrar.Visible:=True;
end;

procedure TIndexGrupos.cargartabla;
  var
    I: integer;
  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.nombre FROM grupos a';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_grupos.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          grd_grupos.Cells[0, I] := CONN.SQL_SELECT.FieldByName('nombre').AsString;

          CONN.SQL_SELECT.Next;
        end;


    end;

    CONN.SQL_SELECT.Close;
  end;

end.
