unit IndexSubCliente;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.Controls.Presentation, RegistroSubCliente;

type
  TFrm_IndexSubCliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line1: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_agregar: TCornerButton;
    btn_configurar: TCornerButton;
    btn_desactivar: TCornerButton;
    Grd_Empresa: TStringGrid;
    col1: TStringColumn;
    col2: TStringColumn;
    col9: TStringColumn;
    col5: TStringColumn;
    col4: TStringColumn;
    col12: TStringColumn;
    col10: TStringColumn;
    col11: TStringColumn;
    col15: TStringColumn;
    col14: TStringColumn;
    col13: TStringColumn;
    col8: TStringColumn;
    Frm_RegistroSubCliente1: TFrm_RegistroSubCliente;

    procedure cargartabla;
    procedure btn_agregarClick(Sender: TObject);
    procedure Frm_RegistroSubCliente1btn_agregarClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    I: integer;

implementation

{$R *.fmx}

uses Datamodule;

procedure TFrm_IndexSubCliente.btn_agregarClick(Sender: TObject);
begin
       Frm_RegistroSubCliente1.Visible := True;
       //frame_Cliente1.limpiardatos;
       Frm_RegistroSubCliente1.cargardatos;
       // frame_Cliente1.cargardatos_municipio;
end;


procedure TFrm_IndexSubCliente.cargartabla;
begin

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS ''ID'',   '+
       'a.nombre AS ''Nombre'',                           '+
       'f.c_UsoCFDI AS ''CFDI'',                          '+
       'a.RFC AS ''RFC'',                                 '+
       'a.telefono AS ''Telefono'',                       '+
       'a.direccion AS ''Direccion'',                     '+
       'a.colonia AS ''Colonia'',                         '+
       'a.cp AS ''CP'',                                   '+
       'b.nombre as ''Pais'',                             '+
       'c.nombre as ''Estado'',                           '+
       'i.nombre AS ''Municipio'',                        '+
       'e.nombre AS ''Cliente''                           '+
       '                                                  '+
       'FROM cliente_sub a                                '+
       'JOIN f4_c_usocfdi f on a.id_cfdi = f.id           '+
       'JOIN paises b on a.id_pais = b.id                 '+
       'JOIN estados c on a.id_estado = c.id              '+
       'JOIN municipios i on a.id_municipio = i.id        '+
       'JOIN clientes e on a.id_cliente = e.id;';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

       for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          Grd_Empresa.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
           Grd_Empresa.Cells[0, I] := CONN.SQL_SELECT.FieldByName('ID').AsString;
          Grd_Empresa.Cells[1, I] := CONN.SQL_SELECT.FieldByName('Nombre').AsString;
          Grd_Empresa.Cells[2, I] := CONN.SQL_SELECT.FieldByName('CFDI').AsString;
          Grd_Empresa.Cells[3, I] := CONN.SQL_SELECT.FieldByName('RFC').AsString;
          Grd_Empresa.Cells[4, I] := CONN.SQL_SELECT.FieldByName('Telefono').AsString;
          Grd_Empresa.Cells[5, I] := CONN.SQL_SELECT.FieldByName('Direccion').AsString;
          Grd_Empresa.Cells[6, I] := CONN.SQL_SELECT.FieldByName('Colonia').AsString;
          Grd_Empresa.Cells[7, I] := CONN.SQL_SELECT.FieldByName('CP').AsString;
          Grd_Empresa.Cells[8, I] := CONN.SQL_SELECT.FieldByName('Pais').AsString;
          Grd_Empresa.Cells[9, I] := CONN.SQL_SELECT.FieldByName('Estado').AsString;
          Grd_Empresa.Cells[10,I] := CONN.SQL_SELECT.FieldByName('Municipio').AsString;
          Grd_Empresa.Cells[11,I] := CONN.SQL_SELECT.FieldByName('Cliente').AsString;


        CONN.SQL_SELECT.Next;
        end;
    end;

end;



procedure TFrm_IndexSubCliente.Frm_RegistroSubCliente1btn_agregarClick(Sender: TObject);

   var
   cfdi, cliente ,pais, estado, municipio : integer;
   nombre, rfc, telefono, direccion, colonia, cp: string;

begin



    cfdi := Frm_RegistroSubCliente1.cb_cfdi.Selected.Index+1;
    cliente := Frm_RegistroSubCliente1.cb_cliente.Selected.Index+1;
    pais := Frm_RegistroSubCliente1.cb_pais.Selected.Index+1;
    estado := Frm_RegistroSubCliente1.cb_estado.Selected.Index+1;
    municipio := Frm_RegistroSubCliente1.cb_municipio.Selected.Index+1;

        //////////////////////////////////////////////////////////////////////
    nombre := Frm_RegistroSubCliente1.edt_nombre.Text;
    rfc := Frm_RegistroSubCliente1.edt_rfc.Text;
    telefono := Frm_RegistroSubCliente1.edt_telefono.Text;
    direccion := Frm_RegistroSubCliente1.edt_direccion.Text;
    colonia := Frm_RegistroSubCliente1.edt_colonia.Text;
    cp := Frm_RegistroSubCliente1.edt_cp.Text;

    ShowMessage(estado.ToString+', '+municipio.ToString+', '+pais.ToString);

      //Esta condici�n se encargar� de detectar que los campos de Registro no se encuentren vacios

      if (cfdi = 0 ) and (cliente = 0) and (pais = 0) and (estado = 0) and (municipio = 0)
       or (nombre = '')  or (rfc = '') or (telefono = '') or (direccion = '') or
          (colonia = '') or (cp = '')  then
      begin
        ShowMessage('�Los campos no pueden estar vacios!');
      end
      else
      begin

        CONN.SQL_SELECT.Close;
        CONN.SQL_SELECT.SQL.Text := 'INSERT INTO clientes_sub '+
                '(nombre, id_cfdi, rfc, telefono, direccion, colonia, cp, id_municipio, id_estado, id_pais, id_cliente,) '+
        'VALUES (:nombre, :id_cfdi, :RFC, :telefono, :direccion, :colonia, :cp, :id_municipio, :id_estado, :id_pais, :id_cliente)';

        CONN.SQL_SELECT.ParamByName('nombre').AsString := nombre;
        CONN.SQL_SELECT.ParamByName('id_cfdi').AsString := cfdi.ToString;
        CONN.SQL_SELECT.ParamByName('RFC').AsString := rfc;
        CONN.SQL_SELECT.ParamByName('telefono').AsString := telefono;
        CONN.SQL_SELECT.ParamByName('direccion').AsString := direccion;
        CONN.SQL_SELECT.ParamByName('colonia').AsString := colonia;
        CONN.SQL_SELECT.ParamByName('cp').AsString := cp;
        CONN.SQL_SELECT.ParamByName('id_municipio').AsString := municipio.ToString;
        CONN.SQL_SELECT.ParamByName('id_estado').AsString := estado.ToString;
        CONN.SQL_SELECT.ParamByName('id_pais').AsString := pais.ToString;
        CONN.SQL_SELECT.ParamByName('id_cliente').AsString := cliente.ToString;



        CONN.SQL_SELECT.ExecSQL;

        if (CONN.SQL_SELECT.RowsAffected > 0) then
          begin
            ShowMessage('�Se ha registrado la informaci�n!');
           Frm_RegistroSubCliente1.Visible:= False;
            cargartabla;
          end
        else
          begin
            ShowMessage('Algo ha salido mal...');
          end;

  end;

end;


end.
