unit Inventario;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ListBox, FMX.Edit, FMX.Objects, FMX.Controls.Presentation, System.Rtti,
  FMX.Grid.Style, FMX.ScrollBox, FMX.Grid;

type
  TFrame7 = class(TFrame)
    pan_Fondo: TPanel;
    rec_Contenido: TRectangle;
    lbl_Inventario: TLabel;
    Line1: TLine;
    lbl_BusquedaINV: TLabel;
    lbl_AlmacenINV: TLabel;
    cb_AlmacenINV: TComboBox;
    grd_Inventario: TStringGrid;
    btn_BuscarINV: TButton;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    procedure cargartabla;
    procedure cargardatos;
    procedure btn_BuscarINVClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses datamodule;
{$R *.fmx}
var almacen : integer;

procedure TFrame7.btn_BuscarINVClick(Sender: TObject);
var I : Integer;
begin
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id FROM almacenes WHERE nombre= :name';

    CONN.SQL_SELECT.ParamByName('name').AsString := cb_AlmacenINV.Selected.Text;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount > 0 then
    begin
      almacen := CONN.SQL_SELECT.FieldByName('id').AsInteger;
    end;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.localizacion_alm, a.existencia, b.nombre as "producto", c.nombre as "medida", d.nombre as "almacen"'+' FROM inventario a'
    +' JOIN productos b on a.id_producto = b.id JOIN unidades_medidas c ON a.id_medida = c.id JOIN almacenes d ON a.id_almacen = d.id'
    +' WHERE a.id_almacen = :almacen;';

    CONN.SQL_SELECT.ParamByName('almacen').AsString := almacen.ToString;
    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_Inventario.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          grd_Inventario.Cells[4, I] := CONN.SQL_SELECT.FieldByName('localizacion_alm').AsString;
          grd_Inventario.Cells[2, I] := CONN.SQL_SELECT.FieldByName('Existencia').AsString;
          grd_Inventario.Cells[1, I] := CONN.SQL_SELECT.FieldByName('Producto').AsString;
          grd_Inventario.Cells[3, I] := CONN.SQL_SELECT.FieldByName('Medida').AsString;
          grd_Inventario.Cells[0, I] := CONN.SQL_SELECT.FieldByName('Almacen').AsString;

          CONN.SQL_SELECT.Next;
        end;

    end
    else
    begin
      grd_Inventario.RowCount := 0;
      grd_Inventario.RowCount := 100;
    end;

end;


procedure TFrame7.cargardatos;
begin

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM almacenes;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_AlmacenINV.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
    cb_ALmacenINV.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;
end;



procedure TFrame7.cargartabla;
var I: integer;

  begin

    {
      Esta funci�n se encarga de cargar la tabla de informaci�n cuando se de
      click al boton de empresas.

      Su primer proceso es realizar un select de la informaci�n que mostrar�
      si esta regresa m�s de un dato, la tabla se llenara con el resultado del
      query

      @Objetos y funciones
       */
          /* TStringGrid
          /* Cells[Columnas, Filas]
       */

    }
    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT a.id, a.localizacion_alm as "Localizacion en Almacen", a.existencia as "Existencia", b.nombre "Producto", c.nombre as "Unidad de Medida", d.nombre AS "Almacen" FROM inventario' + ' a JOIN productos b ON a.id_producto = b.nombre JOIN unidades_medidas c ON a.id_medida = c.abrebiatura JOIN almacenes d ON a.id_almacen = d.nombre';

    CONN.SQL_SELECT.Open;

    if CONN.SQL_SELECT.RecordCount >0 then
    begin

      for I := 0 to CONN.SQL_SELECT.RecordCount - 1 do
        begin
          grd_Inventario.Row := CONN.SQL_SELECT.FieldByName('id').AsInteger;
          grd_Inventario.Cells[0, I] := CONN.SQL_SELECT.FieldByName('Producto').AsString;
          grd_Inventario.Cells[1, I] := CONN.SQL_SELECT.FieldByName('Localizacion en Almacen').AsString;
          grd_Inventario.Cells[2, I] := CONN.SQL_SELECT.FieldByName('Existencia').AsString;
          grd_Inventario.Cells[3, I] := CONN.SQL_SELECT.FieldByName('Unidad de Medida').AsString;
          grd_Inventario.Cells[4, I] := CONN.SQL_SELECT.FieldByName('Almacen').AsString;
        end;
          CONN.SQL_SELECT.Next;
        end;



    end;


end.
