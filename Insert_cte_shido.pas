unit Insert_cte_shido;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects, FMX.Controls.Presentation, FMX.ListBox;

type
  TFrame_Cliente = class(TFrame)
    rec_container: TRectangle;
    lbs_Titulo: TLabel;
    Line2: TLine;
    pan_contenido: TPanel;
    rec_botones: TRectangle;
    btn_configurar: TCornerButton;
    btn_agregar: TCornerButton;
    cb_pais: TComboBox;
    edt_correo: TEdit;
    edt_direccion: TEdit;
    edt_nombre: TEdit;
    lbs_Colonia: TLabel;
    lbs_CP: TLabel;
    lbs_Direccion: TLabel;
    lbs_Pais: TLabel;
    lbs_Nombre: TLabel;
    edt_telefono: TEdit;
    lbs_telefono: TLabel;
    edt_colonia: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edt_rfc: TEdit;
    cb_idtipo: TComboBox;
    Label5: TLabel;
    cb_personatipo: TComboBox;
    Label6: TLabel;
    edt_cp: TEdit;
    cb_empresa: TComboBox;
    Label2: TLabel;
    Label7: TLabel;
    cb_cfdi: TComboBox;
    Label8: TLabel;
    Label1: TLabel;
    cb_estado: TComboBox;
    cb_municipio: TComboBox;
    Fisica: TListBoxItem;
    Moral: TListBoxItem;

    procedure limpiardatos;
    procedure cargardatos;
   // procedure cargardatos_municipio;
   // procedure cb_estadoClosePopup(Sender: TObject);
    procedure cb_estadoClosePopup(Sender: TObject);
//    procedure Label6Click(Sender: TObject);
//    procedure lbs_telefonoClick(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
uses DataModule ;
{$R *.fmx}

procedure TFrame_Cliente.cb_estadoClosePopup(Sender: TObject);
 var
text : String;
begin


 try
    cb_municipio.Items.Clear;
    cb_municipio.Enabled := True;

    CONN.SQL_SELECT.Close;
    CONN.SQL_SELECT.SQL.Text := 'SELECT id, nombre FROM municipios WHERE estado_id = :id';

    CONN.SQL_SELECT.ParamByName('id').AsInteger := cb_estado.Selected.Index+1;
    CONN.SQL_SELECT.Open;

    while not CONN.SQL_SELECT.Eof do
    begin
      cb_municipio.Items.Add(CONN.SQL_SELECT.FieldByName('nombre').AsString);
      cb_municipio.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
      CONN.SQL_SELECT.Next;
    end;

  except on E: Exception do
    cb_Municipio.Enabled := False;
  end;

end;


procedure TFrame_Cliente.limpiardatos;
begin


  //Limpiamos los edit.
  edt_nombre.Text := '';
  edt_correo.Text := '';
  edt_telefono.Text := '';
  edt_rfc.Text := '';
  edt_colonia.Text := '';
  edt_cp.Text := '';
  edt_direccion.Text := '';


  //Limpiamos los Combobox
  cb_personatipo.Items.Clear;
  cb_idtipo.Items.Clear;
  cb_empresa.Items.Clear;
  cb_cfdi.Items.Clear;
  cb_pais.Items.Clear;
  cb_estado.Items.Clear;
  cb_municipio.Items.Clear;



  end;






procedure TFrame_Cliente.cargardatos;
begin


////////////////////////////////////////   Cargar tipo cte/prospecto ////////////////////////////////////////////////
 CONN.SQL_SELECT.Close;


  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.tipo_cliente AS "tipo_cliente" FROM tipo_cliente a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_idtipo.Items.Add(CONN.SQL_SELECT.FieldByName('tipo_cliente').AsString);
    cb_idtipo.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;





////////////////////////////////////////   Cargar Uso CFDI ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.Descripción AS "desc", a.c_UsoCFDI AS "CFDI" FROM f4_c_usocfdi a;';
  CONN.SQL_SELECT.Open;


  while not CONN.SQL_SELECT.Eof do
  begin
       cb_cfdi.Items.Add(CONN.SQL_SELECT.FieldByName('CFDI').AsString + ' - ' + CONN.SQL_SELECT.FieldByName('desc').AsString);
       cb_cfdi.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
       CONN.SQL_SELECT.Next;
  end;


  ////////////////////////////////////////   Cargar Empresas ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM empresa a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_empresa.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_empresa.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;





 ////////////////////////////////////////   Cargar Paises ////////////////////////////////////////////////
  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM paises a;';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_Pais.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_Pais.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;




  ////////////////////////////////////////   Cargar Estados ////////////////////////////////////////////////

  CONN.SQL_SELECT.Close;
  CONN.SQL_SELECT.SQL.Text := 'SELECT a.id AS "id", a.nombre AS "Nombre" FROM estados a WHERE activo = 1';

  CONN.SQL_SELECT.Open;

  while not CONN.SQL_SELECT.Eof do
  begin
    cb_estado.Items.Add(CONN.SQL_SELECT.FieldByName('Nombre').AsString);
    cb_estado.ItemIndex := StrToInt(CONN.SQL_SELECT.FieldByName('id').AsString);
    CONN.SQL_SELECT.Next;
  end;


   //Esta parte solo es para llevar el combo con algo y pueda validar si esta vacio que no tenga cargado algo.
  cb_municipio.Items.Add('');
  cb_municipio.ItemIndex := 0;

end;




end.

